Skip to main content

edX
Main menu
HOW IT WORKSCOURSESSCHOOLSREGISTER NOW
User menu
log in
HarvardX: AI12.1x: Poetry in America: The Poetry of Early New England
Watch the Course Intro Video
School: HarvardX
Course Code: AI12.1x
Classes Start: 31 Oct 2013
Course Length: 4 weeks
Estimated effort: 3-5 hours/week
Prerequisites:

None.




Poetry in America: The Poetry of Early New England

The first module of a course surveying 400 years of poetry in America, from the Puritans to the avant-garde poets of this new century.

About this Course

*Note - This is an Archived course*

This is a past/archived course. At this time, you can only explore this course in a self-paced fashion. Certain features of this course may not be active, but many people enjoy watching the videos and working with the materials. Make sure to check for reruns of this course.

This course will be offered in an experimental format and will not include any graded assignments or offer certificates at the conclusion of the course.

The Poetry of Early New England introduces students to the most important poets of the 17th century, including Anne Bradstreet, Michael Wigglesworth and Edward Taylor. Texts range from theological verse dramas to intimate love poems,  from meditations on nature, family and death to reflections on the civic and communal hopes of the New England migrants. Students will be exposed to relevant poetic forms and traditions,  to rare manuscript and printed materials, and they will have opportunities to practice and refine the way they read, and write about, poems. Course materials include video lectures, roundtable and online discussions--but also interactive exercises and interpretive expeditions to key New England sites.

Poetry in America surveys nearly 400 years of poetry in America.  Treating individual figures (Poe, Whitman, Dickinson, Frost, Williams, Hughes), major poetic movements (Firesides, Modernist, New York, Confessional, L-A-N-G-U-A-G-E) and probing the many uses of poetry across changing times, the course provides a historical time line for the American poetic tradition while also giving students tools to approach any poetic tradition. Who, and what, are poems for? For poets? Readers? To give vent to the soul? To paint or sculpt with words? Alter consciousness? Raise cultural tone? Students will read, engage with, write about and also recite and record American poems.

Before your course starts, try the new edX Demo where you can explore the fun, interactive learning environment and virtual labs. Learn more.

WAYS TO TAKE THIS EDX COURSE:
Simply Audit this Course

Can't commit to all of the lectures, assignments, and tests? Audit this course and have complete access to all of the course material, tests, and the online discussion forum. You decide what and how much you want to do.
Free to AllMore about Auditing a Course
Course Staff


Elisa New
Elisa New is the Powell M. Cabot Professor of American Literature at Harvard University where she teaches classic American literature from Anne Bradstreet through Marilynne Robinson and from the Puritans to the present day. She is the author of The Regenerate Lyric: Theology and Innovation in American Poetry (Cambridge University Press, 1992) The Lines Eye: Poetic Experience, American Sight (Harvard University Press, 1999) Jacob's Cane: A Jewish Family's Journey from the Four Lands of Lithuania to the Ports of London and Baltimore (2009). She also has two books forthcoming from Wiley-Blackwell, New England Beyond Criticism: In Defense of America's First Literature (2014) and How To Read American Poetry (2015).


Seth Herbst
Seth Herbst is a doctoral candidate in the English Department at Harvard University, where he studies early modern literature and its connections to music. Seth's dissertation focuses, in particular, on the poet John Milton and his visionary representation of music in Paradise Lost, Paradise Regained, and the major lyric poems. Other interests include (in the Renaissance) Shakespeare and Spenser, as well as (in later time periods) Dickinson and Yeats. After graduating from Harvard College in 2008, Seth spent a year teaching English in rural southern Japan under the auspices of the Japan Exchange and Teaching Program (JET).


Leah Reis-Dennis
Leah Reis-Dennis graduated from Harvard College in 2013, where she studied American History and Literature. At Harvard, Leah studied with Elisa New, whose "Poetry in America" course inspired her love for American poetry. Leah works for HarvardX as a Lead Course Developer for AI12X.


Dave Weimer
Dave Weimer is working toward a Ph.D. in English at Harvard University. He researches primarily literary responses to disestablishment in the nineteenth century but he also studies the history of blindness and writing by and about the blind in the US in the nineteenth century. Dave has taught courses both at Harvard and also through the Boston University Prison Education Program. Before starting graduate school, he taught high school in the Mississippi Delta.


Sharon Howell
Sharon Howell is the Dean of Adams House in Harvard College, and a Lecturer in History and Literature.  Her B.A. in English is from Connecticut College, her M.A. from Villanova, and her PhD from Harvard. Her other poetry exploits include a podcast—ramblebarrow, and a book—Girl in Everytime (Pressed Wafer Press 2011).  Her passion for all American poets has been stoked anew by working on Poetry in America, and she's thrilled to be part of the development team. Her particular wish is to help high school teachers and students to use the course's offerings and tools to enrich their own poetry classwork.


Ari Hoffman
Ari Hoffman is a PhD candidate in English at Harvard University, where   he serves as a Resident Tutor in Lowell House. He studies contemporary fiction, with a focus on Jewish literature. Ari’s dissertation examines the ways in which   Jewish American authors have represented Israel as well as the relationship   between American Jews and Zionism more broadly. A 2010 graduate of Harvard   College, Ari has studied at Cambridge University and traveled extensively in India  and the Middle East. He is thrilled to be on board for this exciting endeavor!


John North Radway
John North Radway teaches and writes about poetry in the broad context of modern and postmodern American literature and culture. He is finishing a dissertation about the fate of epic in twentieth-century poetry, examining the afterlife of an ancient literary tradition amid the turbulent literary and social climate of the past hundred years. A graduate of Amherst College, he writes novels and poetry of his own when he thinks no one’s looking.


Carra Glatt
Carra Glatt is a PhD candidate in the Harvard English Department, where she focuses on the Victorian novel. She is working on a dissertation on counterfactuals in the 19th century novel, examining how considerations of unrealized narrative possibilities can shape our understanding of texts. Carra has been a Teaching Fellow for several Harvard College classes, including Elisa New's "Fictions of America," and has worked with students from a variety of backgrounds and disciplines as a tutor at the Harvard Extension School.

Prerequisites

None.


About UsJobsNewsFAQContact
EdX offers interactive online classes and MOOCs from the world’s best universities. Online courses from MITx, HarvardX, BerkeleyX, UTx and many other universities. Topics include biology, business, chemistry, computer science, economics, finance, electronics, engineering, food and nutrition, history, humanities, law, literature, math, medicine, music, philosophy, physics, science, statistics and more. EdX is a non-profit online initiative created by founding partners Harvard and MIT.

Facebook
Twitter
Google+
LinkedIn
Meetup
© 2014 edX, except where noted, all rights reserved.
Terms of Service and Honor Code
Privacy Policy (Revised 11/04/2013)