Skip to main content

edX
Main menu
HOW IT WORKSCOURSESSCHOOLSREGISTER NOW
User menu
log in
LouvainX: Louv3.01x: Decouvrir la science politique
Watch the Course Intro Video
School: LouvainX
Course Code: Louv3.01x
Classes Start: 17 Feb 2014
Course Length: 6 weeks
Estimated effort: 2 to 3 hours/week
Prerequisites:

This course will be taught in French. Ce cours sera enseigné en français.



Decouvrir la science politique

Décoder des enjeux politiques actuels à partir d’une boîte à outils de concepts fondamentaux et de postures critiques de la science politique.
About this Course

Le politique nous concerne tous: chaque jour, des décisions publiques sont adoptées dans une série de secteurs, impactant notre quotidien. Dans quel contexte ces décisions sont-elles adoptées ? Au 21ème siècle, de nombreux paradoxes brouillent la vision que nous pouvons en avoir. Qu’est-ce que le pouvoir dans une société ‘multi-échelles’ ? L’État demeure-t-il un acteur politique majeur s’il est concurrencé par des organisations internationales et régionales ? La démocratie prônée par la communauté internationale peut-elle coexister avec des régimes autoritaires voire totalitaires? Quel est le rôle des citoyens face aux idéologies ? Le cours vise à comprendre et à expliquer des phénomènes politiques à partir d’une posture d’analyse critique.

Avant, pendant, et après le cours, consultez les pages Facebook et Google+ pour les annonces et les discussions.

Politics impact all of us: everyday political decisions concern many areas of our lives. In which context are these decisions made? In the 21st century, many paradoxes blur our vision of these decisions. What is power in a multi-level society? Does the state remain the main political actor, if it is in competition with international and regional organizations? Can democracy – pushed forward by the international community – coexist with authoritarian, or even totalitarian, regimes? What is the role of citizens in regard to ideologies? The course aims at understanding and explaining political phenomena from a critical standpoint. 

Before, during, and after the course, check out the official Facebook and Google+ pages for announcements and discussions.

WAYS TO TAKE THIS EDX COURSE:
Simply Audit this Course

Can't commit to all of the lectures, assignments, and tests? Audit this course and have complete access to all of the course material, tests, and the online discussion forum. You decide what and how much you want to do.
Free to AllMore about Auditing a Course
or
Try for a Certificate

Looking to test your mettle? Participate in all of the course's activities and abide by the edX Honor Code. If your work is satisfactory, you'll receive a personalized certificate to showcase your achievement.
Free to AllMore about Honor Code Certificates
Course Staff


Nathalie Schiffino
Nathalie Schiffino est docteur en science politique, orientation gouvernement et administration publique, de l’Université catholique de Louvain. Professeur, elle y dispense des enseignements et dirige des recherches relatifs aux théories de la démocratie (traitement des crises, représentation et participation), à l’analyse et à l’évaluation des politiques publiques (régulation de secteurs à risque). Elle a publié des résultats de ses travaux notamment dans International Review of Administrative Science , Revue internationale de politique comparée , Journal of Risk Research , West European Politics , German Policy Studies, Politique et Sociétés, Éthique publique ainsi que des ouvrages dont Crises politiques et démocratie en Belgique dans la collection « Logiques politiques » chez L’Harmattan. Elle a également contribué à plusieurs livres collectifs publiés chez Routledge, Lexington ou Académia. Elle est présidente de la commission doctorale en sciences politiques et sociales de l’Académie Louvain et présidente de jurys en baccalauréat et masters.
 

Nathalie Schiffino is a professor of political science at the Catholic University of Louvain. She earned a M.A. and a PhD in government and public administration from the Catholic University of Louvain. Prof. Schiffino’s research and courses concern theories of democracy (crises, representation and participation) as well as policy analysis (risk regulation, evaluation). The results of her research have been published in the International Review of Administrative Science, Revue internationale de politique comparée, Journal of Risk Research, West European Politics, German Policy Studies, Politique et Sociétés, and Éthique publique, among others. She is the author and co-author of several books published by Routledge, Lexington, l’Harmattan and Académia. She is the president of the PhD commission, and the juries in Bachelor and Master of political science.

 


Pierre Baudewyns
Pierre Baudewyns est diplômé en histoire et en science politique de l’Université catholique de Louvain. Professeur, il y enseigne la science politique et la sociologie politique. Ses recherches sont centrées sur les comportements politiques en Belgique et en Europe dans une perspective comparée. Les résultats des recherches ont été publiés dans différentes revues et livres dont West-European Politics, Comparative European Politics, Revue Internationale de Politique Comparée. Dans le cadre de ses activités, il est également co-directeur du Belgian National Election Study, secrétaire général de la Revue Internationale de Politique Comparée et vice-Président de l’Association belge francophone de science politique (ABSP).
 

Pierre Baudewyns holds history and political science degrees from the Catholic University of Louvain, where he teaches political science and political sociology. His research interest topic concerns political behavior among the masses and elites in Belgium and Europe. He has published in West European Politics, Comparative European Politics, and Revue Internationale de Politique Comparée. He is currently co-director of Belgian National Election Studies (the French seaking part), Vice President of the French-speaking Belgian political science association (ABSP) and General Secretary of the Revue Internationale de Politique Comparée.


Vincent Legrand
Vincent Legrand est docteur en science politique et diplômé en langue arabe et islamologie. Professeur à l’Université catholique de Louvain (UCL), il y donne un cours d’introduction à la science politique à destination des étudiants en 1ère année en Economie & Gestion et en Ingénieur de Gestion, ainsi que des enseignements au niveau Master dans le domaine des sciences politiques, économiques et sociales du monde arabe contemporain. Ses recherches, publiées notamment dans la Revue française de science politique, Politique et sociétés et Studia Diplomatica, portent sur la prise de décision en politique étrangère, l’autoritarisme, les mouvements sociaux, les processus révolutionnaires et de démocratisation dans les pays arabes, le 'religious peace-building,' ainsi que sur les questions liées aux relations entre religion et politique, notamment au niveau de l'Union européenne (UE).
 

Vincent Legrand graduated with a PhD in Political Science, as well as in Arab & Islamic Studies. He is a professor of political science at Université catholique de Louvain (UCL). He teaches an introductory course on political science to students graduating in Economics & Management Studies at the bachelors level, and also lectures in the field of contemporary Arab studies at the masters level. He does research in the field of foreign policy decision-making, social movements, revolutionary processes, authoritarianism and democratization in the Arab world, religious peace-building, as well as religion and politics in the EU. The results of his research have been published in books and periodicals such as Revue française de science politique, Politique et sociétés and Studia Diplomatica.

 


Min Reuchamps
Min Reuchamps est diplômé de l’Université de Liège (Belgique) et de Boston University. Il est professeur à l’Université catholique de Louvain où il enseigne la science politique, les modèles socio-politiques de l'État ainsi que la théorie et méthode de la décision politique. Ses recherches portent sur le fédéralisme et la gouvernance multi-niveaux, la sociologie politique ainsi que les méthodes participatives et délibératives et ont été publiées dans plusieurs ouvrages et articles parus notamment dans Regional and Federal Studies, Territory, Politics, Governance, Politique et Sociétés et la Revue internationale de politique comparée. Par ailleurs, il est le co-secrétaire de l'Association belge francophone de science politique (ABSP) et le coordinateur de la cellule méthodologie du G1000, une initiative citoyenne de démocratie délibérative.
 

Min Reuchamps holds political science degrees from the Université de Liège (Belgium) and Boston University (USA). He is professor of political science at the Université catholique de Louvain where he teaches introduction to political science, the socio-political models of the state as well as the theory and method of political decision-making. His research interests are federalism and multi-level governance, political sociology and deliberative and participatory methods. He has published several books and articles in Regional and Federal Studies, Territory, Politics, Governance, Politique et Sociétés and the Revue internationale de politique comparée. He is currently the co-secretary of the French-speaking Belgian political science association (ABSP) and the G1000’s methodology coordinator, a citizens-based initiative of deliberative democracy.

Prerequisites

This course will be taught in French. Ce cours sera enseigné en français.

About UsJobsNewsFAQContact
EdX offers interactive online classes and MOOCs from the world’s best universities. Online courses from MITx, HarvardX, BerkeleyX, UTx and many other universities. Topics include biology, business, chemistry, computer science, economics, finance, electronics, engineering, food and nutrition, history, humanities, law, literature, math, medicine, music, philosophy, physics, science, statistics and more. EdX is a non-profit online initiative created by founding partners Harvard and MIT.

Facebook
Twitter
Google+
LinkedIn
Meetup
© 2014 edX, except where noted, all rights reserved.
Terms of Service and Honor Code
Privacy Policy (Revised 11/04/2013)