Skip to main content

edX
Main menu
HOW IT WORKSCOURSESSCHOOLSREGISTER NOW
User menu
log in
UC BerkeleyX: Stat2.2x: Introduction to Statistics: Probability

School: UC BerkeleyX
Course Code: Stat2.2x
Classes Start: 12 Apr 2013
Course Length: 5 weeks
Prerequisites:

High school arithmetic, good comprehension of English, knowledge of the material of Stat2.1x, with emphasis on histograms, averages, SDs, and the normal curve.




Introduction to Statistics: Probability

An introduction to probability, with the aim of developing probabilistic intuition as well as techniques needed to analyze simple random samples.

About this Course

*Note - This is an Archived course*

This is a past/archived course. At this time, you can only explore this course in a self-paced fashion. Certain features of this course may not be active, but many people enjoy watching the videos and working with the materials. Make sure to check for reruns of this course.

Statistics 2 at Berkeley is an introductory class taken by about 1000 students each year. Stat2.2x is the second of three five-week courses that make up Stat2x, the online equivalent of Berkeley's Stat 2.

The focus of Stat2.2x is on probability theory: exactly what is a random sample, and how does randomness work? If you buy 10 lottery tickets instead of 1, does your chance of winning go up by a factor of 10? What is the law of averages? How can polls make accurate predictions based on data from small fractions of the population? What should you expect to happen "just by chance"? These are some of the questions we will address in the course.

We will start with exact calculations of chances when the experiments are small enough that exact calculations are feasible and interesting. Then we will step back from all the details and try to identify features of large random samples that will help us approximate probabilities that are hard to compute exactly. We will study sums and averages of large random samples, discuss the factors that affect their accuracy, and use the normal approximation for their probability distributions.

Be warned: by the end of Stat2.2x you will not want to gamble. Ever. (Unless you're really good at counting cards, in which case you could try blackjack, but perhaps after taking all these edX courses you'll find other ways of earning money.)

As Stat2.2x is part of a series, the basic prerequisites are the same as those for Stat2.1x: high school arithmetic and good comprehension of English. In addition, you are expected to know the material of Stat2.1x, with particular emphasis on histograms, averages, SDs, and the normal curve.
The fundamental approach of the series was provided in the description of Stat2.1x and appears here again: There will be no mindless memorization of formulas and methods. Throughout the course, the emphasis will be on understanding the reasoning behind the calculations, the assumptions under which they are valid, and the correct interpretation of results.

WAYS TO TAKE THIS EDX COURSE:
Simply Audit this Course

Can't commit to all of the lectures, assignments, and tests? Audit this course and have complete access to all of the course material, tests, and the online discussion forum. You decide what and how much you want to do.
Free to AllMore about Auditing a Course
Course Staff


Ani Adhikari
Ani Adhikari, Senior Lecturer in Statistics at UC Berkeley, has received the Distinguished Teaching Award at Berkeley and the Dean's Award for Distinguished Teaching at Stanford University. While her research interests are centered on applications of statistics in the natural sciences, her primary focus has always been on teaching and mentoring students. She teaches courses at all levels and has a particular affinity for teaching statistics to students who have little mathematical preparation. She received her undergraduate degree from the Indian Statistical Institute, and her Ph.D. in Statistics from Berkeley.


Philip B. Stark
Philip B. Stark is Professor of Statistics at University of California, Berkeley where he developed the university's first online course. He has published research on the Big Bang, causal inference, the census, earthquake prediction, election auditing, food web models, the geomagnetic field, geriatric hearing loss, information retrieval, Internet content filters, nonparametrics, the seismic structure of Sun and Earth, spectroscopy, spectrum estimation, and uncertainty quantification for computational models of complex systems. He has consulted for the U.S. Departments of Agriculture, Commerce, Housing and Urban Development, Justice, and Veterans Affairs; the Federal Trade Commission; the California and Colorado Secretaries of State; the California Attorney General; and the Illinois State Attorney. He has testified to Congress and the California legislature, and in litigation concerning employment, environmental protection, equal protection, lending, intellectual property, jury selection, import restrictions, insurance, natural resources, product liability, trade secrets, and advertising. He received his AB from Princeton University and his Ph.D. from UCSD.


Paul Matsiras
Paul Matsiras is a graduating senior double majoring in statistics and economics at the University of California, Berkeley. In addition to his studies, Paul is an officer for Cal's Undergraduate Economics Association, as well as the President and Founder of the school's Undergraduate Statistics Association. Outside of school, he has interned at both the Federal Reserve Board of Governors and the U.S. Department of Commerce. And, after completing his undergraduate degree, Paul intends to pursue a graduate degree in economics.


Gordon A. Heaton
Gordon A. Heaton is a second year undergraduate at UC Berkeley, intending to double major in Economics and Statistics. He is an international student from London, UK, although he was born in Tokyo, Japan. He has taken coursework in Probability and Statistics, Multivariable Calculus, Game Theory, Macroeconomic Theory, and Microeconomic Theory. Gordon is fascinated by the prospect of applying quantitative methods to relevant issues, and this is what drew him to Statistics. He ultimately hopes to do research in Economics or some related field, and is currently considering continuing education in graduate school. Gordon is an avid soccer player, and in his free time enjoys photography and waterskiing.

Prerequisites

High school arithmetic, good comprehension of English, knowledge of the material of Stat2.1x, with emphasis on histograms, averages, SDs, and the normal curve.

FAQs

What is the format of the class?
Instruction will be consist of brief lectures and exercises to check comprehension. Grades (Pass or Not Pass) will be decided based on a combination of scores on short assignments, quizzes, and a final exam.

How much does it cost to take the course?
Will the text of the lectures be available?
Do I need to watch the lectures live?
Will certificates be awarded?
Can I contact the Instructor or Teaching Assistants?
Do I need any other materials to take the course?

About UsJobsNewsFAQContact
EdX offers interactive online classes and MOOCs from the world’s best universities. Online courses from MITx, HarvardX, BerkeleyX, UTx and many other universities. Topics include biology, business, chemistry, computer science, economics, finance, electronics, engineering, food and nutrition, history, humanities, law, literature, math, medicine, music, philosophy, physics, science, statistics and more. EdX is a non-profit online initiative created by founding partners Harvard and MIT.

Facebook
Twitter
Google+
LinkedIn
Meetup
© 2014 edX, except where noted, all rights reserved.
Terms of Service and Honor Code
Privacy Policy (Revised 11/04/2013)