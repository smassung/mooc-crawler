require 'open-uri'

output = File.open("edx.txt", 'w')
url = "https://www.edx.org/course-list?page="
for i in 0..11
  `sleep 1`
  html = open("#{url}#{i}").read
  puts " Crawling page #{i}"
  for m in html.scan(/<h2 class="title course-title">[^<]*<strong><a href="[^"]*">([^<]*)<\/a><\/strong><\/h2>/)
    for title in m
      output << title << "\n"
      puts "  #{title}"
    end
  end
end
