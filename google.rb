require 'open-uri'

base_url = "https://www.google.com/search?q="
cache_base = "http://www.textise.net/showText.aspx?strURL="
cache_base += "http://webcache.googleusercontent.com/search?q=cache:"
site = "site:khanacademy.org"
agent = "Mozilla/5.0 (Windows; U; Windows NT 5.1; de; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3"

urls = File.open("urls.txt", 'w')
mooc = "khan"

for course in File.open("#{mooc}.txt.part", 'r').readlines
  `sleep 2`
  file = course.gsub(/ |\t/, "_")
  file.gsub!(/[^a-zA-Z0-9_]/, "")
  course = course.chomp.gsub(/ |\t/, "%20")
  url = "#{base_url}#{site}%20#{course}"
  puts "Searching #{url}"
  begin
    html = open(url, "User-Agent" => agent).read
    first_result = html.match(/<h3 class="r"><a href="([^"]*)"/).captures[0]
    first_result = first_result.match(/(http[^&]*)&/).captures[0]
    puts " -> found #{first_result}"
    urls << first_result << "\n"
    `phantomjs text-scraper.js #{first_result} #{mooc}/#{file}`
  rescue
    puts " -> error!"
  end
end
