require 'open-uri'

base = "https://www.khanacademy.org"
paths = File.open("khan-topics.txt", 'r').readlines
output = File.open("khan.txt", 'w')
for path in paths
  `sleep 1`
  url = "#{base}#{path.chomp}"
  html = open(url).read
  topic = path.gsub(/.*\//, "").chomp
  begin
    for m in html.scan(/<h4 class="topic-title">([^<]*)<\/h4>/)
      for title in m
        puts " Found #{topic}: #{title}"
        output << topic << "\t" << title << "\n"
      end
    end
  rescue
    puts " -> error on #{path}"
    next
  end
end
