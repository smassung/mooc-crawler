

Sign In
Sign Up
Course Catalog
 
View Trailer
Intro to Theoretical Computer Science
Dealing with Challenging Problems
  
 Intermediate Join 22,472 Students
Class Summary

This class teaches you about basic concepts in theoretical computer science -- such as NP-completeness -- and what they imply for solving tough algorithmic problems.

What Will I Learn?

At the end of this course, you will have a solid understanding of theoretical computer science. This will not only allow you to recognize some of the most challenging algorithmic problems out there, but also give you powerful tools to deal with them in practice.

What Should I Know?

You should have a basic understanding of algorithms (such as CS215) and programming (such as CS101). No prior knowledge about theoretical computer science required!

Syllabus

Lesson 1: Challenging Problems

An introduction to tough problems and their analysis

Lesson 2: Understanding Hardness

What we mean when a problem is “hard” and the concept of NP-completeness

Lesson 3: Showing Hardness

Tools to let you recognize and prove that a problem is hard

Lesson 4: Intelligent Force

Smart techniques to solve problems that should – theoretically – be impossible to solve

Lesson 5: Sloppy Solutions

Gaining speed by accepting approximate solutions

Lesson 6: Poking Around

Why randomness can be of help – sometimes. An introduction to complexity classes.

Lesson 7: Ultimate Limits

Problems that no computer can ever solve. In theory.

FAQ

When does the course begin?

This class is self paced. You can begin whenever you like and then follow your own pace. It’s a good idea to set goals for yourself to make sure you stick with the course.

How long will the course be available?

This class will always be available!

How do I know if this course is for me?

Take a look at the “Class Summary,” “What Should I Know,” and “What Will I Learn” sections above. If you want to know more, just enroll in the course and start exploring.

Can I skip individual videos? What about entire lessons?

Yes! The point is for you to learn what YOU need (or want) to learn. If you already know something, feel free to skip ahead. If you ever find that you’re confused, you can always go back and watch something that you skipped.

How much does this cost?

It’s completely free! If you’re feeling generous, we would love to have you contribute your thoughts, questions, and answers to the course discussion forum.

What are the rules on collaboration?

Collaboration is a great way to learn. You should do it! The key is to use collaboration as a way to enhance learning, not as a way of sharing answers without understanding them.

Why are there so many questions?

Udacity classes are a little different from traditional courses. We intersperse our video segments with interactive questions. There are many reasons for including these questions: to get you thinking, to check your understanding, for fun, etc... But really, they are there to help you learn. They are NOT there to evaluate your intelligence, so try not to let them stress you out.

What should I do while I’m watching the videos?

Learn actively! You will retain more of what you learn if you take notes, draw diagrams, make notecards, and actively try to make sense of the material.

Start free courseware
Course Instructors


Sebastian Wernicke
INSTRUCTOR
Sebastian studied Bioinformatics at Universität Tübingen and holds a Ph.D. from Universität Jena in Germany, where his research focused on finding efficient algorithms for computationally hard problems on biological networks. After several years of strategic consulting for pharma companies and financial services, he's currently working with Seven Bridges Genomics, a big data bioinformatics startup. He is also well-known for his TED Talks, especially the one on the statistics of TED Talks..


Sean Bennett
COURSE DEVELOPER
Sean Bennett is a Course Architect at Udacity and is passionate about using the web to improve the quality of education available worldwide. Sean's background is in web programming, and he likes to dabble in functional web programming. When he's not working to improve education, Sean likes running, hiking, and preparing for the inevitable zombie apocalypse.


Sarah Norell
COURSE DEVELOPER
Sarah Norell holds a PhD in Mathematics from the University of London, UK. She has lectured at the London School of Economics, University of Umeå and Mid-Sweden University, and tutored all ages. Her breadth of experience at different levels has brought insight into some of the underlying problems students have with mathematics and it is this insight she brings to the courses here at Udacity.


INFORMATION

What We Offer 
Help and FAQ 
Feedback Program

COMMUNITY

Blog 
Meetups 
News & Media

UDACITY

About 
Jobs 
Contact Us 
Legal

FOLLOW US ON

   
© 2011-2014 Udacity, Inc.

