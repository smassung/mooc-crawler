|Global Partners
CoursesSpecializationsNewInstitutionsAbout ▾ |Sign InSign Up

21st Century American Foreign Policy
What is American foreign policy? Who makes it? Why is it the way it is? How does it affect the rest of the world? Professor Bruce Jentleson has taught the subject for over 30 years, written one of the leading books on it, and has served in numerous U.S. foreign policy positions.

Preview Lectures

Watch intro video
About the Course
U.S. foreign policy has a huge impact on the world. That impact is widely debated among Americans and by people in many countries. Our goal in this course is to gain more understanding of what U.S. foreign policy is, who makes it, why is it the way it is, and how it affects the rest of the world. Through lectures, readings, and other course elements we build on the information available from the media and other such sources while delving deeper into the issues, their history, their broader context and major debates. We seek to do so as a community of learning committed not only to the curricular objectives for our course, but seeking to demonstrate to the world how to conduct informed and respectful discourse on subjects which have plenty of room for disagreement.

The first week of the course provides an overview and analytic framework, and focuses in on the process and politics by which U.S. foreign policy is made. Each of the next five weeks is geared to U.S. foreign policy in different regions: Asia with a particular focus on U.S.-China relations; the Middle East and such issues as Iran, Arab-Israeli peace, Syria, and the Arab uprisings; relations with Western Europe and Russia; relations in the Western Hemisphere with Latin America and Canada; and key issues in U.S.-Africa relations. While we cannot cover all issues, we do try for a balance of breadth and depth.
Course Syllabus
Six weeks organized into six units:

Course Overview, Analytic Framework and the Making of U.S. Foreign Policy
Asia’s Rising Strategic Importance: U.S. Relations with China and in the Asia-Pacific Region
War, Peace, Terrorism, Democracy:  Old and New Challenges in the Middle East
Old Friends, Old Enemy: 21st Century Relations with Europe and Russia
The Americas: Relations with Latin America and Canada
Africa: Persisting Old Issues, Pressing Newer Ones
Recommended Background
Strong interest in world politics and international affairs is the most important requirement. Prior relevant knowledge from one’s own education and any professional work experience would be great, both for you and for our community of learning.
Suggested Readings
The course draws extensively on my book American Foreign Policy: The Dynamics of Choice in the 21st Century (New York: W.W. Norton and Company, 5th edition 2013), just published in its 5th edition. Two chapters are available on our course website; if interested, the book can be purchased from W.W. Norton or various others. Selected articles and other relevant readings will also be assigned.
Course Format
Each week is structured as a unit on one of the topics listed on the syllabus. Each of these units will have four or five components consisting of a video lecture, background readings, quizzes and other learning reinforcement exercises. We also will use the message board and other mechanisms for dialogue among the students and with the professor and teaching team.
FAQ
Will I get a Statement of Accomplishment after completing this class?

Yes. Students who successfully complete the class will receive a Statement of Accomplishment signed by the instructor.

What resources will I need for this class?

For this course, all you need is an Internet connection and the time to read, write, discuss, and think about our topic.

Is this course only for Americans?

Definitely not. American foreign policy affects people all around the world. This is an opportunity to learn more about it, and for our community of learning to be a truly global one sharing ideas, viewpoints and aspirations.

Will this be just like watching CNN International, BBC, al Jazeera or other such news sources? 

While we of course will address many issues of the day, we seek to delve more deeply into the issues, their history, their broader context and major debates. 

What is the coolest thing I'll learn if I take this class? 
Everyone has their own conception of what’s “cool”; for me it includes learning more about the world in which we live.

Sessions

Join for Free
Course at a Glance
6 weeks
4-6 hours of work / week
English
English subtitles
Earn official recognition for your work

Verified Certificate
Instructors

Bruce W Jentleson
Duke University
Categories
Social Sciences
Share

Related Courses

Understanding Europe: Why It Matters and What It Can Offer You

Age of Jefferson

Creative Problem Solving
Browse more courses
Coursera empowers people to improve their lives, the lives of their families, and the communities they live in with education.

© 2014 Coursera Inc. All rights reserved.

COMPANY

About
People
Leadership
Careers
FRIENDS

Partners
Community
Programs
CONNECT

Google+
Twitter
Facebook
Blog
Store
MORE

Terms
Privacy
Help
Press
Contact
