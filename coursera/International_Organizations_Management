|Global Partners
CoursesSpecializationsNewInstitutionsAbout ▾ |Sign InSign Up

International Organizations Management
Part of the "Challenges in Global Affairs" Specialization »
This course provides an overview of the management challenges international organizations & NGOs are faced with. You will learn key theoretical frameworks and practical tools to excel in this environment.


About the Course
International and not-for-profit organizations present an increasingly complex environment to work in and therefore require for their successful management an unprecedented level of managerial skills on top of a deep understanding of the socioeconomic and political context they operate in. This course is designed to provide students with (1) basic notions of the practice of international relations (2) a general overview of the management challenges international and not-for-profit organizations are faced with as well as key theoretical frameworks and practical tools for managers to excel in this environment. Key areas of management will be reviewed, from strategy setting to implementation through marketing & fund raising, and assessment. (3) Given the growing interaction between public and private sectors, this course also touches upon the management of public/private partnerships. 

This course is coordinated by Dr. Gilbert Probst, Professor, Organization and Management, and Co-Director of the Executive-MBA program at the University of Geneva. 
A panel of instructors, all visiting professors at the University of Geneva’s International Organizations MBA, will be invited to give you a general overview of international and not-for-profit organizations. Among them are: 

Dr. Sebastian Buckup, Director and alumnus of the Global Leadership Fellows Programme at the World Economic Forum in Geneva, Switzerland, with previous experience at the ILO, UNDP and the private sector.
Julian Fleet has experience and training in both public health and in law. He currently serves as Director of the Department of Human Resources Management in the Joint United Nations Programme on HIV/AIDS (UNAIDS).
Claudia Gonzalez, Head of Marketing for the Global Fund to fight AIDS, TB and malaria, with more than 18 years leading marketing, PR, branding, social media and communications for international organizations like the World Economic Forum and UNHCR. 
Dr. Bruce Jenks served as Assistant Secretary General at UNDP until 2010 and in 2013 he was appointed by the Secretary General to be a member of the Council of the University for Peace. In his career, he served as Director of the Office of Strategic Planning (1997-99), as Director of the UN Office in Brussels (1995-97) Director of Budget and Chief of Staff (1990-95). Since 2010, Dr. Jenks has consulted with a number of Organizations. Dr. Jenks is an adjunct professor at the Columbia University School of International and Public Affairs, and lectures at the Geneva Graduate Institute of International and Development Studies and at the University of Geneva.
Stephan Mergenthaler is Associate Director, Strategic Foresight at the World Economic Forum in Geneva, as well as Non-resident Fellow with the Global Public Policy Institute in Berlin (GPPi).
Dr. Lea Stadtler is Research Fellow at the University of Geneva, DHEC, as well as Visiting Scholar at INSEAD and has previous work experience in the private sector.       
Prof. Gilbert Probst is Managing Director, Leadership Office and Academic Affairs, and Dean of the Global Leadership Fellows Program at the World Economic Forum. He is also a full professor for organizational behavior and management and Co-director of the Executive-MBA program at the University of Geneva. Professor Probst is an award-winning author and editor of a number of books and articles in prestigious journals (Organization Science, Academy of Management Executive, and European Management Journal). He served as a consultant for various major companies such as Kuoni, Holcim, Hewlett Packard, Siemens, Winterthur Insurance, Deutsche Bank, DaimlerChrysler or Swisscom, and has been a board member for Holcim, Kuoni and several SMEs.
Course Syllabus
Week 1: Introduction to International Organizations and NGOs (Julian Fleet)
Week 2: The Shifting Context for International Organizations (Stefan Mergenthaler and Sebastian Buckup)
Week 3: Public Private Partnerships (Gilbert Probst and Lea Stadler)
Week 4: Leadership in the UN System (Bruce Jenks)
Week 5: Marketing & Fundraising (Claudia Gonzalez)
Recommended Background
Graduate students of international relations/business management/international law, aspiring to join an international or not-for-profit organization and/or professionals with a minimun of 3 years of relevant work experience. 
Course Format
The class consists of 5 lessons over the period of 5 weeks. Each weekly lesson is comprised of a series of lecture videos, which are between 15 and 30 minutes in length. 
Most videos will contain 1-2 integrated quiz questions. 
A quiz for each week's lesson will serve to recap and strengthen your retention of the key learning objectives.
FAQ
Will I get a Statement of Accomplishment after completing this class? 
Yes. Students who successfully complete the quizzes and participate to the forum activity for week 2 will receive a Statement of Accomplishment signed by Prof. Gilbert Probst. However, no credits will be awarded.
Sessions

Join for Free
Earn a Verified Certificate
Course at a Glance
5 weeks
2-3 hours of work / week
English
English subtitles
Earn official recognition for your work

Verified Certificate
Specialization Certificate
Instructors

Gilbert Probst
University of Geneva
Sebastian Buckup
University of Geneva
Julian Fleet
University of Geneva
Claudia Gonzalez
University of Geneva
Bruce Jenks
University of Geneva
Stephan Mergenthaler
University of Geneva
Lea Stadtler
University of Geneva
Categories
Business & Management
Share


Related Courses

Understanding Europe: Why It Matters and What It Can Offer You

The Changing Global Order

Configuring the World: A Critical Political Economy Approach
Browse more courses
Coursera empowers people to improve their lives, the lives of their families, and the communities they live in with education.

© 2014 Coursera Inc. All rights reserved.

COMPANY

About
People
Leadership
Careers
FRIENDS

Partners
Community
Programs
CONNECT

Google+
Twitter
Facebook
Blog
Store
MORE

Terms
Privacy
Help
Press
Contact
