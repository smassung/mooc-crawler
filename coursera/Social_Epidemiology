|Global Partners
CoursesSpecializationsNewInstitutionsAbout ▾ |Sign InSign Up

Social Epidemiology
Social epidemiology is about how a society makes people sick and/or healthy. We address not only the identification of new disease risk factors (e.g., deficient social capital) but also how well-known exposures (e.g., cigarette smoking, lead paint, health insurance) emerge and are maintained by the social system.


Watch intro video
About the Course
This course is about understanding the determinants of health from a broad perspective. We focus on how social relationships and institutions -- such as familial relationships, national policies, and global economic forces -- promote or undermine the health of populations. The course covers existing evidence of health disparities, research methods, and theories relevant to the topic. 

The course is interesting because it reveals the so-called fundamental causes of disease and health disparities recognizable within social groups. For example, we examine why a flu germ can affect whole groups of people differently. In short, the course challenges the notion that health is a narrowly defined medical problem. 

Students in the course will listen to lectures, read provided materials, and complete quizzes and tests that examine comprehension and one's ability to synthesize ideas. 

Upon completion of the course students should:
have a deep appreciation for how social arrangements impact the health of populations,
be able to critically evaluate the scientific and popular health literature that address the causes of disease,
be able to measure key social drivers such as race and socioeconomic status, and 
be able to conceive of research strategies that can answer questions critical for policy making.
Course Syllabus
Week 1:  Background and History 
What is social epidemiology and where did it come from? What is different about it? 

Week 2: Issues 
What are the fundamental issues (e.g., environment, race, genetics) in/for social epidemiology? 

Week 3: Health Disparities  
How can social epidemiology improve our understanding of the identification and analysis of, if not remedies for, health disparities? 

Week 4: Theories and Constructs 
What theories and/or constructs are fundamental to social epidemiology? 

Week 5: Measurement 
What are some fundamental measurement issues in social epidemiology? 

Week 6: Design & Inference 
What are some fundamental design and analysis tools in social epidemiology? 

Week 7: Doing Things 
What social epidemiological interventions work and fail, and why? 
Recommended Background
Students should be curious, enthusiastic readers, with undergraduate-level analytical and critical thinking skills; and have an interest in policy implications surrounding social and societal health issues. A basic understanding of statistics would also be useful.  
Course Format
The class will consist of 7 lectures, one per week. Each lecture will be made up of 2-4 video modules or parts. Each of the video modules will be about 8-12 minutes in length. 

Most of the video modules (i.e., parts of the lectures) will have a mini quiz of one or two multiple choice questions. There will also be standalone readings and (group) homework assignments that are not part of video lectures. Additionally, there will be an overall lecture quiz for each of the seven lectures. This overall quiz will have approximately 10 multiple choice questions in it. There will not be a final exam. 
FAQ
Will I get a Statement of Accomplishment for this course?  
Yes. Students who complete the course will receive a Statement of Accomplishment signed by the instructor.
No sessions available
Add to Watchlist
Course at a Glance
2-4 hours of work / week
English
English subtitles
Instructors

Michael Oakes
University of Minnesota
Categories
Health & Society
Social Sciences
Share

Related Courses

Epidemiology: The Basic Science of Public Health

Global Health: An Interdisciplinary Overview

Epidemics - the Dynamics of Infectious Diseases
Browse more courses
Coursera empowers people to improve their lives, the lives of their families, and the communities they live in with education.

© 2014 Coursera Inc. All rights reserved.

COMPANY

About
People
Leadership
Careers
FRIENDS

Partners
Community
Programs
CONNECT

Google+
Twitter
Facebook
Blog
Store
MORE

Terms
Privacy
Help
Press
Contact
