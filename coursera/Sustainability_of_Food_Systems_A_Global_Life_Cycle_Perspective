|Global Partners
CoursesSpecializationsNewInstitutionsAbout ▾ |Sign InSign Up

Sustainability of Food Systems: A Global Life Cycle Perspective
This course explores the diversity of the foods we eat, the ways in which we grow, process, distribute, and prepare them, and the impacts they have upon our environment, health, and society. We will also examine the challenges and opportunities of creating a more sustainable global food system in the future.


Watch intro video
About the Course
Hamburger or hummus? Organic or conventional? Mediterranean diet or McDonald's? What dietary choices are truly the most sustainable when we consider that what we eat affects not just our health but also the environment and the well-being of others?

This course will provide you with an overview of our world’s food system and its many impacts from the individual to the global scale. You will gain further appreciation of the complex implications of choices that are made along the food supply chain. You will be challenged to think critically about how the global food system may need to change to adapt to future economic and environmental conditions.

Our world’s population is projected to grow both in numbers and in wealth in the coming decades. Meeting future food demand, and doing so in a sustainable fashion, will require that we greatly increase the amount we produce and improve the efficiency by which it makes it to our tables. Join us as we explore today’s global food system and how we might improve its sustainability for our benefit and that of future generations.
Course Syllabus
This course is organized around fifteen questions that span our world's food system, its impacts, and its future.

What is food?
What foods do we eat?
How and where did our food originate?
How and where do we grow our food?
How do we facilitate the trade and transfer of our food?
How do we process and distribute our food?
How do we prepare our food?
How do we choose what to eat?
How do we assess the sustainability of the global food system?
How does what we eat affect our health?
How does what we eat affect our environment?
How much food will we need in the future?
What challenges do we face in reforming the global food system?
How can we provide more food more sustainably?
How can we each contribute to a more sustainable global food system?
Recommended Background
No background is required.
Course Format
You will investigate current topics in food sustainability and have the opportunity to participate in discussions with your colleagues, who themselves will be from all over the world. Other members of the University of Minnesota faculty with expertise in these areas will assist us in answering the questions around which the course is organized. Case studies, readings, and other resources will emphasize that responsible decisions about what to eat require that we consider the entire global food supply chain and its full set of economic, environmental, and social consequences.
FAQ
Will I get a Statement of Accomplishment for this course? 
Yes. Students who complete the course will receive a Statement of Accomplishment signed by the instructor.

What resources will I need for this course?
You will need nothing other than an internet connection and an enthusiastic attitude!
No sessions available
Add to Watchlist
Course at a Glance
3-4 hours of work / week
English
English subtitles
Instructors

Jason Hill
University of Minnesota
Categories
Economics & Finance
Food and Nutrition
Energy & Earth Sciences
Share

Related Courses

Economic Issues, Food & You

Sustainable Agricultural Land Management

Growing Old Around the Globe
Browse more courses
Coursera empowers people to improve their lives, the lives of their families, and the communities they live in with education.

© 2014 Coursera Inc. All rights reserved.

COMPANY

About
People
Leadership
Careers
FRIENDS

Partners
Community
Programs
CONNECT

Google+
Twitter
Facebook
Blog
Store
MORE

Terms
Privacy
Help
Press
Contact
