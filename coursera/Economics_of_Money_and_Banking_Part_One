|Global Partners
CoursesSpecializationsNewInstitutionsAbout ▾ |Sign InSign Up

Economics of Money and Banking, Part One
Introduction to a “money view” of economic activity for modern times, building on the intellectual traditions of British central banking and American institutionalism. Part One explores the economics of payment systems and money markets. Part Two explores connections with foreign exchange and capital markets.


Watch intro video
About the Course
The last three or four decades have seen a remarkable evolution in the institutions that comprise the modern monetary system. The financial crisis of 2007-2009 is a wakeup call that we need a similar evolution in the analytical apparatus and theories that we use to understand that system. Produced and sponsored by the Institute for New Economic Thinking, this course is an attempt to begin the process of new economic thinking by reviving and updating some forgotten traditions in monetary thought that have become newly relevant.

Three features of the new system are central.

Most important, the intertwining of previously separate capital markets and money markets has produced a system with new dynamics as well as new vulnerabilities. The financial crisis revealed those vulnerabilities for all to see. The result was two years of desperate innovation by central banking authorities as they tried first this, and then that, in an effort to stem the collapse.  

Second, the global character of the crisis has revealed the global character of the system, which is something new in postwar history but not at all new from a longer time perspective.  Central bank cooperation was key to stemming the collapse, and the details of that cooperation hint at the outlines of an emerging new international monetary order. 

Third, absolutely central to the crisis was the operation of key derivative contracts, most importantly credit default swaps and foreign exchange swaps. Modern money cannot be understood separately from modern finance, nor can modern monetary theory be constructed separately from modern financial theory. That's the reason this course places dealers, in both capital markets and money markets, at the very center of the picture, as profit-seeking suppliers of market liquidity to the new system of market-based credit.


Course Syllabus
Introduction
  1:  The Four Prices of Money
  2:  The Natural Hierarchy of Money
  3:  Money and the State:  Domestic
  4:  The Money View, Macro and Micro
Banking as a Clearing System
  5:  The Central Bank as a Clearinghouse
  6:  Federal Funds, Final Settlement
  7:  Repos, Postponing Settlement
  8:  Eurodollars, Parallel Settlement
Banking as Market Making
  9:  The World that Bagehot Knew
  10:  Dealers and Liquid Security Markets
  11:  Banks and the Market for Liquidity
  12:  Lender/Dealer of Last Resort

Recommended Background
The Barnard version of this course requires Intermediate Microeconomics and Intermediate Macroeconomics as prerequisites for economics majors, but non-economics majors (such as engineers and historians) without those prerequisites have taken the course and done fine. The important thing is some familiarity with economic reasoning and concepts and perhaps some familiarity with the subject matter (such as through job experience or an internship).  
Suggested Readings
The lectures are meant to be complete in themselves, and there is no assigned textbook for the course. Weekly readings by a variety of different authors will be posted to introduce students to the range of discourse on money. Students may wish to purchase my book The New Lombard Street, How the Fed Became the Dealer of Last Resort (Princeton 2011), which is also meant to be complete in itself, as a backstop for the videos.
Course Format
The class will consist of lecture videos, shot live in the classroom but then edited down into digestible segments, with integrated quiz questions and animated slide videos added. There will also be weekly quizzes and a final exam.
FAQ
What is the coolest thing I'll learn if I take this class? You will learn to read, understand, and evaluate professional discourse about the current operation of money markets at the level of the Financial Times.  
Sessions

Join for Free
Course at a Glance
7 weeks
5-7 hours of work / week
English
English subtitles
Instructors

Perry G Mehrling
Columbia University
Categories
Economics & Finance
Share

Related Courses

Financial Markets

Teaching Character and Creating Positive Classrooms

Applying to U.S. Universities
Browse more courses
Coursera empowers people to improve their lives, the lives of their families, and the communities they live in with education.

© 2014 Coursera Inc. All rights reserved.

COMPANY

About
People
Leadership
Careers
FRIENDS

Partners
Community
Programs
CONNECT

Google+
Twitter
Facebook
Blog
Store
MORE

Terms
Privacy
Help
Press
Contact
