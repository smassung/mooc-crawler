|Global Partners
CoursesSpecializationsNewInstitutionsAbout ▾ |Sign InSign Up

Principles of Macroeconomics
All of us are affected by macroeconomic forces – they shape the very world we live in. And governments all around the world try to shape those forces in ways that (hopefully) improve the lives of their constituents. In this subject, we will examine the major theories used by macro economists to analyse national economies and the international economy.


Watch intro video
About the Course
In this course, we will examine the main bodies of economic theory that have been used to guide economists’ and policy makers’ understanding of the macroeconomy. Macroeconomics is a word derived from the Greek prefix “makros”, meaning large. It is the study of economic aggregates, of national and international economies and of the economic management role played by governments and international organisations.

The founder of modern macroeconomics, the British economist John Maynard Keynes, famously wrote “The ideas of economists and political philosophers, both when they are right and when they are wrong are more powerful than is commonly understood. Indeed, the world is ruled by little else. Practical men, who believe themselves to be quite exempt from any intellectual influences, are usually slaves of some defunct economist.” This will be the key theme we explore in the course, the influence of theory and the way that policy responses to events like recession, unemployment, inflation and the Global Financial Crisis, reflect that theory.

We will cover important macroeconomic concepts such as the national accounts, unemployment and inflation, explain how the ideas associated with John Maynard Keynes laid the foundation for governments’ active management of the macroeconomy in the post war era, and examine the intellectual basis for the monetary and fiscal policy responses to the Global Financial Crisis.  Economists’ approaches to the analysis of long run economic growth will also be considered. The approach will be critical. There are few areas of human endeavour that attract so much debate and disagreement as the management of a modern industrial economy. We will not shy away from those debates. But what we will do is emphasise the systematic theorising that underlies the way macroeconomists and policy makers approach their task. The aim is to enable those who take the course to become informed analysts of the macroeconomy and of current (and past) macroeconomic policy debates.

Image source: Nilss Olekalns University of Melbourne 2012

Suggested Readings
There are no required readings for this course.
No sessions available
Add to Watchlist
Course at a Glance
6-8 hours of work / week
English
English subtitles
Instructors

Nilss Olekalns
The University of Melbourne
Categories
Economics & Finance
Share

Related Courses

Live!: A History of Art for Artists, Animators and Gamers

Principles of Microeconomics

Microeconomics Principles
Browse more courses
Coursera empowers people to improve their lives, the lives of their families, and the communities they live in with education.

© 2014 Coursera Inc. All rights reserved.

COMPANY

About
People
Leadership
Careers
FRIENDS

Partners
Community
Programs
CONNECT

Google+
Twitter
Facebook
Blog
Store
MORE

Terms
Privacy
Help
Press
Contact
