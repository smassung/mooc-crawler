|Global Partners
CoursesSpecializationsNewInstitutionsAbout ▾ |Sign InSign Up

Understanding Russians: Contexts of Intercultural Communication
Learn about the intercultural communication process using Russian –Western communication as an example. Look at the interrelations between different contexts (cultural, institutional, professional, social, interpersonal) of communication focusing on cultural history and national psychology of Russians.


Watch intro video
About the Course
The main focus of this course is to look at the interrelations between different types of contexts (cultural, institutional, professional, social, interpersonal and others) within the intercultural communication process using Russian – Western communication as an example.

The purpose of the course is to provide the students with a broad overview of the basic principles governing past, present and future interactions between Russia and the West focused on the culture and national psychology of Russians and Western Europeans.

This course looks at the cases when Russian basic cultural values show up through linguistic choices shaping language production which is consequently misattributed by Western partners.  No matter what the language of intercultural communication is - Russian, or English - the meaning of many linguistic expressions may be reconstructed wrongly by the representatives of another culture.

 

We will tackle some basic questions:

What are concepts of culture that have most influence on communication?
What are Russian basic cultural values and how they shape modern Russian consciousness?
 What are specific communication patterns of modern Russian, including that of public and electronic discourse?
What is important to know about communication with Russians in organizational contexts?

This course is NOT just a list of instructions of what to do and not to do while dealing with Russians
Course Syllabus
Module 1. Introduction to the course.  

Week 1.  Overview of the concepts “culture” and “communication”. Why “Russianness?”  Importance of linguistic approaches. The role of ICC contexts.  Structure of the course.



Module 2. Culture in Intercultural Communication.  

Week 2. Metaphors of culture.  Beliefs, values and stereotypes. Cultural homogeneity.  How much culture can be learned?  How culturally relative one can be? Basic values of Russian culture placed within the cultural dimensions, history and geography.  Generalized knowledge vs. stereotypes of “enigmatic Russian soul.”

 

Module 3.  Communication as Interaction.

Week 3. Language as a primary source of communication and miscommunication in ICC.  Shared knowledge as a prerequisite for communication.  Cooperation and Politeness principles in Russian communication

Week 4. Context and contexts of human interaction.  ”Contextual clues’ in Russian communication. Russian cultural models. Interpersonal aspect of ICC with Russians.

 

Module 4. Communication with Russians in various discourse systems

Week 5.  Communication with Russians in corporate discourse systems.  Introduction of the market economy and post-soviet management. Old fears and new anxieties. Technocrats and managers.  Language of management in Russia.

Week 6.  Communication with Russians in professional contexts.   Occupational cultures. Social stratification and new elites.  Russia’s middle-classes, professional associations and civil society. Case study of an international educational program.

Week 7. Communication with Russians in gender and generational discourse systems.   Gender roles in modern Russian society.  Cross-gender communication in social and business environments.  Family relations.

Week 8. Stories of Russian culture and everyday communication within an in-group. Types of stories that characterize cultures. Official, stories, cultural stories, conversational stories. Humor and jokes, obscene language. 

 Week 9. Course wrap up.  Reflections on intercultural communication within the course

Recommended Background
We are looking for students who are curious to learn how to deal with Russians, who want to make sense of their personal encounters with Others. Love for Russian culture and language is a plus, but not obligatory.

You may be required to integrate your knowledge of humanities and social sciences in intercultural approach to communication.

Intercultural sensitivity is a pre-requisite for taking this course and enhancing it is its goal.
Suggested Readings
Although the lectures are designed to be self-contained, we recommend (but do not require) that students refer to the books:

1.  Holden, N. and Cooper, С., J. Carr (1998). Dealing with the New Russia: Management Cultures in Collision. Wiley. (selected chapters).

2.  Ron Scollon, Suzanne Wong  Scollon, Rodney H. Jones (2012). Intercultural communication : a discourse approach. Malden, Mass., Blackwell Publishers.(selected chapters).

2. Wierzbicka, A. (1997). Understanding Cultures Through Their Key Words: English, Russian, Polish, German and Japanese. (Oxford: OUP). (selected chapters).

Course Format
The class will consist of lecture videos, which are around 15 minutes in length. They will be followed by 2-3 questions for review.  Lecture notes will be available.  

During the course you will be asked to participate in discussions based on lectures and course literature. 

At the end of the course there will be a final exam in the form of problem solving critical incidents of (mis)communication with Russians.
FAQ
1. Will I get a Statement of Accomplishment after completing this class?

Yes. Students who successfully complete the class will receive a Statement of Accomplishment signed by the instructor.



2. What is the coolest thing I'll learn if I take this class?

You will be able to produce your own definition of the “enigmatic Russian soul” and communicate with it.

 

3. What resources will I need for this class?

For this course, all you need is an Internet connection and willingness to engage in intercultural communication and to discover the world of Russian culture.

Sessions

Join for Free
Course at a Glance
9 weeks
4-6 hours of work / week
English
English subtitles
Instructors

Mira Bergelson
Higher School of Economics
Categories
Humanities 
Share

Related Courses

Practicing Tolerance in a Religious Society: The Church and the Jews in Italy

Latin American Migration

Understanding Europe: Why It Matters and What It Can Offer You
Browse more courses
Coursera empowers people to improve their lives, the lives of their families, and the communities they live in with education.

© 2014 Coursera Inc. All rights reserved.

COMPANY

About
People
Leadership
Careers
FRIENDS

Partners
Community
Programs
CONNECT

Google+
Twitter
Facebook
Blog
Store
MORE

Terms
Privacy
Help
Press
Contact
