|Global Partners
CoursesSpecializationsNewInstitutionsAbout ▾ |Sign InSign Up

Genes and the Human Condition (From Behavior to Biotechnology)
To acquire an understanding of the fundamental concepts of genomics and biotechnology, and their implications for human biology, evolution, medicine, social policy and individual life path choices in the 21st century.


Watch intro video
About the Course
In this class we will cover the essentials of genomics to help you better understand your own life (know thy genome, know thyself), and how advances in biotechnology are radically changing the scientific landscape.

We will begin with some of the most basic issues of genetics, such as the composition of genomes and how the information in them is processed so we can get an adult human from an egg. Then we’ll see that much of the power of genomics is in comparisons. If I only had your genome there's not much I could do with it. But if I lined it up against a chimpanzee genome I’d see that they are 98.5% identical. It’s that 1.5% difference that we study to determine what makes a chimp a chimp and a human a human. 

In the next few years you’ll be able to get your genome done for a few hundred dollars, and we will look at the challenges you might face dealing with this new self-knowledge. We shall particularly focus on behavior. How can genes predispose some of us to be nice and others of us to be psychopaths? We will be examining behavior as a complex unfolding of interactions between your genome and the environment.

Thanks in large part to genomics we are beginning to understand the living processes that constitute ourselves, where we can begin to intervene to take control of our own future. We will look at the technologies involved and the consequences of this new power. Genetic engineering has already changed what you eat and the medicines you take, and will likely therefore change how long you live. If we choose to genetically engineer ourselves as some advocate then we may also change how we view ourselves as human beings. We do not know where our ability to intervene in our own living processes is going to lead us. Depending on your point of view that is the promise or the threat of the era we live in. Scientists today already envisage creating life from scratch, and we are entering a world where political considerations, and imagination, may be the only brakes on biotechnology.
Course Syllabus
Lectures

Week 1. An Introduction to Genetics and Learning the Lingo  

Week 2. A History Lesson-Comparative Genomics, know thy ancestors  

Week 3. My Genes Made Me Do It, An examination of the role of genes in behavior, and midterm exam (from weeks 1-3 material)

Week 4. My Genes Didn’t Make Me Do It. Beyond The Genes, Nature via Nurture, and Epigenetics  

Week 5. An Introduction to Genetic Engineering and Learning the Lingo  

Week 6. The Future is Now, Biotechnology and Final Exam (non-cumulative)
Learning Outcomes

Students will be able to describe/demonstrate:

I. A conceptual and holistic understanding of genetics that includes the major questions arising at the intersection of genetics, genomics and society. The course will stress human genetics.

II. An understanding of terms, concepts, and approaches used in studying genetics and genomics. As a case study, the course will examine behavioral genetics: James Watson likes to say “the 20th century saw the fusion of genetics and biochemistry, so the 21st century will see the fusion of genetics and psychology.”

III. An understanding of the power of comparative genomics. In this aspect, the course will focus particularly on human evolution as illuminated by the genomes of chimpanzees and several extinct human kinds including the Neanderthals.

IV. An understanding of the scientific, political, legal, social, economic and ethical issues raised by genomics and the application of biotechnology to “transhumanism.”

Recommended Background
All are welcome to join us in this adventure. Some previous exposure to biology will enrich your experience with this course.

Suggested Readings
Although the lectures are designed to be self-contained, we will be recommending additional readings throughout the course for those who wish to delve further into particular topics. All additional readings are open source materials which can be found through the web. You do not need to purchase anything.
Course Format
The class consists of  five to seven 10-15 minute talks per week. The videos contain integrated quiz questions. A midterm examination will be available as will a final examination. These exams will consist of multiple choice questions.

FAQ
Will I get a certificate after completing this class?  Yes. Students who successfully complete the course will receive a certificate signed by the instructors.
No sessions available
Add to Watchlist
Course at a Glance
5-8 hours of work / week
English
English subtitles
Instructors

Raymond J. St. Leger
University of Maryland, College Park
Tammatha O'Brien
University of Maryland, College Park
Categories
Medicine
Health & Society
Biology & Life Sciences
Share

Related Courses

Useful Genetics Part 1

Useful Genetics Part 2

Introduction to Genetics and Evolution
Browse more courses
Coursera empowers people to improve their lives, the lives of their families, and the communities they live in with education.

© 2014 Coursera Inc. All rights reserved.

COMPANY

About
People
Leadership
Careers
FRIENDS

Partners
Community
Programs
CONNECT

Google+
Twitter
Facebook
Blog
Store
MORE

Terms
Privacy
Help
Press
Contact
