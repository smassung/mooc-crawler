|Global Partners
CoursesSpecializationsNewInstitutionsAbout ▾ |Sign InSign Up

A Beginner's Guide to Irrational Behavior
Behavioral economics couples scientific research on the psychology of decision making with economic theory to better understand what motivates financial decisions. In A Beginner’s Guide to Irrational Behavior, you will learn about some of the many ways in which we behave in less than rational ways, and how we might overcome our shortcomings. You’ll also learn about cases where our irrationalities work in our favor, and how we can harness these human tendencies to make better decisions.


Watch intro video
About the Course
This course will draw heavily on my own research, and pulls largely from my three books: Predictably Irrational (2008), The Upside of Irrationality (2010), and The Honest Truth About Dishonesty (2012). We will examine topics such as our “irrational” patterns of thinking about money and investments, how expectations shape perception, economic and psychological analyses of dishonesty by honest people, how social and financial incentives work together (or against each other) in labor, how self-control comes into play with decision making, and how emotion (rather than cognition) can have a large impact on economic decisions. This highly interdisciplinary course will be relevant to all human beings. 

The goals of this class:
Introduce you to the range of cases where people (consumers, investors, managers, friends, significant others, and even you) might make decisions that are inconsistent with standard economic theory and the assumptions of rational decision making. This is the lens of behavioral economics.
Help you think creatively about the applications of behavioral economics to the development of new products, technologies and public policy, and to understand how business and social policy strategies could be modified with a deeper understanding of the effects these principles have on all of us.
Course Syllabus
There are two tracks that you can take to receive a Statement of Accomplishment for this course. For each track, you will need to earn a grade of 85% or above, but your path toward that grade will be different under each track.

Normal: 
50% Lecture Quizzes
50% Final Exam

Distinction: 
 20% Reading Quizzes
 20% Lecture Quizzes
 30% Final Exam
 15% "Solve a Problem" Writing Assignment
 15% "Design an Experiment" Writing Assignment
Recommended Background
Curiosity about human nature.
Suggested Readings
I will cover some of the material that is in my 3 books Predictably Irrational, Revised and Expanded Edition: The Hidden Forces That Shape Our Decisions (2008), The Upside of Irrationality: The Unexpected Benefits of Defying Logic at Work and at Home (2010), and The Honest Truth About Dishonesty: How We Lie to Everyone---Especially Ourselves (2012).  

And in honor of this class, all three books will be available as an e-bundle. You can purchase the Irrational Bundle through Chegg, Kindle, Nook, iBookstore,  Kobo, or Google.
Course Format
Precourse:       Introduction to Behavioral Economics

Week 1:           Irrationality

Week 2:           Psychology of Money

Week 3:           Dishonesty

Week 4:           Labor and Motivation

Week 5:           Self-control

Week 6:           Emotion

Postcourse:    Applications
FAQ
Will I get a Statement of Accomplishment after completing this class?
Yes. Students who complete the class with a grade of 85% and above will receive a Statement of Accomplishment signed by Dan.

Sessions

Join for Free
Course at a Glance
8 weeks
7-11 hours of work / week
English
English, Portuguese, Russian subtitles
Instructors

Dan Ariely
Duke University
Categories
Economics & Finance
Humanities 
Health & Society
Business & Management
Social Sciences
Share

Related Courses

Music's Big Bang: The Genesis of Rock 'n' Roll

Reason and Persuasion: Thinking Through Three Dialogues By Plato

Bioinformatic Methods II
Browse more courses
Coursera empowers people to improve their lives, the lives of their families, and the communities they live in with education.

© 2014 Coursera Inc. All rights reserved.

COMPANY

About
People
Leadership
Careers
FRIENDS

Partners
Community
Programs
CONNECT

Google+
Twitter
Facebook
Blog
Store
MORE

Terms
Privacy
Help
Press
Contact
