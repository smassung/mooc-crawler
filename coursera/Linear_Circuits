|Global Partners
CoursesSpecializationsNewInstitutionsAbout ▾ |Sign InSign Up

Linear Circuits
Learn the analysis of circuits including resistors, capacitors, and inductors. This course is directed towards people who are in science or engineering fields outside of the disciplines of electrical or computer engineering.


Watch intro video
About the Course
This course is a first introduction to electrical systems. It includes the analysis of circuits including resistors, capacitors, and inductors with DC and AC sources in the time domain and in the frequency domain. Laboratory demonstrations are given to reinforce the concepts learned from the lectures and homework. The course is targeted at people with a scientific or technical background who are not electrical or computer engineers. The coverage is not as deep as a circuits course aimed at electrical engineers. There are a number of physical applications demonstrated in this course that serve to motivate this topic to a wider audience. The course is ideal for someone who wants to gain a basic understanding of electrical circuits, someone who wants to get better intuition for what they have already learned, precocious hobbyists, or for someone who is considering electrical engineering as a career.
Course Syllabus
This course is broken into five Modules, each having associated homework and a quiz: 
Module 1: Background (1 week): 
Background information on electricity, resistors, and circuit diagrams

Module 2: Resistive Circuits (2 weeks): 
Ohm's Law, Kirchhoff's Law, Resistors in series and in parallel, Systematic Solution Methods, Physical applications, and Lab demos

Module 3: Reactive Circuits (2 weeks) 
Capacitors, Inductors, First and Second Order Differential Equations, RC and RL and RLC circuit steady-state and transient response to a DC source, Physical applications, Lab demos

Module 4: Frequency Analysis (2 weeks): 
AC response, Impedance, Transfer functions, Frequency response, Filtering, Applications, Lab demos

Module 5 (1 week): 
Real and reactive power, power factor, transformers


Each module has a quiz associated with it, where the modules that take longer are assigned more credit in grading.  Each week has a homework associated with it that is due at the end of the week. 
Recommended Background
Completed a first course in calculus, have a working knowledge of matrices and linear algebra, complex numbers, been introduced to basic circuit elements at the level commonly covered in a college physics course
Suggested Readings
The lectures are designed to be self-contained. A optional reference book is Fawwaz Ulaby and Michel Maharbiz, "Circuits," (2nd edition), National Technology & Science Press, 2010, ISBN-13: 978-1-934891-10-0. Students who wish to perform the optional laboratory experiments can purchase the book bundled with the National Instruments myDAQ board. Various purchasing options are available from the vendor at http://www.studica.com/mydaq
Course Format
This class will consist of lecture videos, most of which are between 10 and 12 minutes in length. There will be required homework assignments and quizzes and optional laboratory assignments.
FAQ
Is this a stand-alone course?
Yes, however, we intend to produce a follow-up course for Electronics that is also aimed at scientific and technical people who are from fields outside of electrical or computer engineering. Look for that one to be offered in early 2014.
Sessions

Join for Free
Course at a Glance
9 weeks
5-7 hours of work / week
English
English subtitles
Instructors

Bonnie H. Ferri
Georgia Institute of Technology
Categories
Engineering
Share

Related Courses

Digital Systems - Sistemas Digitales: De las puertas lógicas al procesador

Электричество и магнетизм. Часть 1 (Electricity and Magnetism. Part 1)

Fundamentals of Electrical Engineering Laboratory
Browse more courses
Coursera empowers people to improve their lives, the lives of their families, and the communities they live in with education.

© 2014 Coursera Inc. All rights reserved.

COMPANY

About
People
Leadership
Careers
FRIENDS

Partners
Community
Programs
CONNECT

Google+
Twitter
Facebook
Blog
Store
MORE

Terms
Privacy
Help
Press
Contact
