|Global Partners
CoursesSpecializationsNewInstitutionsAbout ▾ |Sign InSign Up

Imagining Other Earths
Are we alone? This course introduces core concepts in astronomy, biology, and planetary science that enable the student to speculate scientifically about this profound question and invent their own solar systems.


Watch intro video
About the Course
Over the past two decades, astronomers have discovered over a thousand planets around nearby stars.  Based on our current knowledge, it seems likely that there are millions of stars in the Galaxy that host Earth-sized planets in Earth-like orbits. What is the range of conditions for these planets to host life? In this course, students will engage with a wide range of concepts in astronomy, biology, chemistry, geology and physics with a focus on developing the background they will use need to think further about this profound question. We will explore the origin and evolution of  life on Earth, particularly in extreme environments, the properties of planets and moons in our Solar System,  the properties of stars and the newly discovered extrasolar planets.

Course assignments include two short papers describing proposed space missions to study nearby planets and to search for extrasolar planets and a final paper. In the final paper, students will have an opportunity to invent their own planetary system and describe it in terms of either the astronomy of how it was discovered, the properties of their planet and its host star, or the biology of life in the system. Papers will be circulated and evaluated by fellow students as part of the learning experience in the course; this will provide opportunities to develop students' abilities to think like a scientist by applying principles of scientific thinking, to learn new ideas from other students, and to creatively make new connections across different sciences and parts of the course.
Course Syllabus
Introductory Lectures
Lecture 1: The Universe is Big!
Lecture 2: What is life?
The Solar System
Lecture 3: Energy balance: What determines planetary temperature?
Lecture 4: Snowball Earth
Lecture 5: Planetary Atmospheres
Lecture 6: Earth, Venus and Mars: the goldilocks story
Lecture 7: Mars and the search for life
Lecture 8: The Moon and tides
Lecture 9: Titan and Europa: habitable moons? 
Assignment 1: Report on Planetary Exploration Missions
Stars and Extrasolar Planets

Lecture 10: Kepler’s Laws Radial Velocity Searches for Planets
Lecture 11: Kepler Mission
Lecture 12: Imaging Other Planets
Lecture 13: Life of Stars
Lecture 14: Origin of the Elements
Lecture 15: Stellar Atmospheres Lecture 16: Jovian Planets 
Assignment 2: Report on Astronomy Missions
Earth and the Origin of Life

Lecture 17: Formation History of the Earth and the Origin of Water
Lecture 18: Proteins, RNA, and DNA: Origin of Life
Lecture 19: Tree of Life
Lecture 20: Evolution
Lecture 21: Sex and the Origin of Complex Life
Lecture 22: Extremophiles
Lecture 23: Extinctions and Evolution Imagining Other Planets
Lecture 24: Aliens and the Future of Space Travel 
Final Assignment: Design your own planetary system
Recommended Background
High school level algebra, biology, chemistry and physics

Suggested Readings
James Kasting, How to Find a Habitable Planet L. Weinstein and J.A. Adam,Guesstimation Neil F Comins,What if the Earth had Two Moons?
Useful websites:
http://planetquest.jpl.nasa.gov/
http://exoplanet.eu/
http://www.astrobiology.com
http://astrobiology.nasa.gov/
http://www.princeton.edu/astrobiology/
http://www.pbs.org/wgbh/nova/space/finding-life-beyond-earth.html
Course Format
Two lectures per week.

Two 5 page reports on proposed NASA and ESA exploration missions.

Final paper (5-10 pages) describing the properties of  a planetary system that you invent.

FAQ
Does Princeton award credentials or reports regarding my work in this course?
No certificates, statements of accomplishment, or other credentials will be awarded in connection with this course.

Sessions

Join for Free
Course at a Glance
12 weeks
5-7 hours of work / week
English
English subtitles
Instructors

David Spergel
Princeton University
Categories
Physics
Energy & Earth Sciences
Share

Related Courses

Confronting The Big Questions: Highlights of Modern Astronomy

Beauty, Form & Function: An Exploration of Symmetry

Digital Systems - Sistemas Digitales: De las puertas lógicas al procesador
Browse more courses
Coursera empowers people to improve their lives, the lives of their families, and the communities they live in with education.

© 2014 Coursera Inc. All rights reserved.

COMPANY

About
People
Leadership
Careers
FRIENDS

Partners
Community
Programs
CONNECT

Google+
Twitter
Facebook
Blog
Store
MORE

Terms
Privacy
Help
Press
Contact
