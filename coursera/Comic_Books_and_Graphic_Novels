|Global Partners
CoursesSpecializationsNewInstitutionsAbout ▾ |Sign InSign Up

Comic Books and Graphic Novels
Comic books have arrived! "Comic Books and Graphic Novels" presents a survey of the comic book canon and of the major graphic novels in circulation today. Its governing question is simple: by what terms can we discuss comic books as literary art? In pursuit of that question it develops a theory of literary reading and time itself. Visit us at www.facebook.com/UCBComics Reddit Username: Kuskin (staff cpav13)


Watch intro video
About the Course
The comic book pamphlet developed as an independent literary form in the 1930s and early 1940s and has been a favorite of adolescent enthusiasts and cult devotees ever since. Recently, it has entered into a process of transformation, moving from a species of pulp fiction on the margins of children’s literature to an autonomous genre, one Will Eisner labeled the graphic novel. This transformation has been noted in such literary venues as the New York Times and the New Yorker, as well as in an increasing number of university classrooms and bookstore shelves. 

“Comic Books and Graphic Novels” presents a survey of the history of American comics and a review of major graphic novels circulating in the U. S. today. It is focused on three main points. First, it argues that as comics develop in concert with, and participate in literary culture, they should be considered literature. Second, it reasons that such a designation forces us to redefine our concept of literature itself. Finally, it explores this transformative literary world by arguing that comics have much to teach us about ourselves. 

Get started by enrolling in an upcoming session, then print out the official course playset and get started! 

Course Syllabus
Syllabus
Comic Books and Graphic Novels
Professor William Kuskin
University of Colorado Boulder
This is the final schedule. A final syllabus will be available when the course opens.

SCHEDULE

WEEK ONE: WELCOME TO THE COURSE(RA) 

Video 1: Welcome to the Course

Video 2: The Syllabus (Overview) 
Video 3: The Syllabus (Logistics)

Video 4: What is a Comic?

Video 5: Teaching Comics (w/Barry Barrows) 

Video 6: Collecting (w/Jim Vacca) 
WEEK TWO: TERMS AND CONDITIONS 

Lecture 1a: Reading the Grid 
Lecture 1b: Two Comics Masters

Lecture 2a: The Golden Age 
Lecture 2b: Golden Age Master 
Lecture 3: The Virulent Art   

WEEK THREE: CRASH AND REBIRTH

Lecture 4a: Big Brother Steps In 
Lecture 4b: The Comics Code Authority

Video 7: Fredric Wertham and the Atomic Age (w/Jim Vacca)

Lecture 5a: The Silver Ages Rises 
Lecture 5b: Silver Age Master 
Lecture 6a: Underground Comix 
Lecture 6b: Black and White 
Video 8: Comics Economics (w/Wayne Winsett) 
WEEK FOUR: PATERNITY AND CREATIVITY

Lecture 7a: The Death of the Father 
Lecture 7b: The Birth of the Children 
Lecture 8: Art Spiegelman’s Maus 
a: An American Artist 
b: How to Read a 

Book 9: Alison Bechdel’s Fun Home 
a: Finding Yourself in a Book 
b: What Defines Art

WEEK FIVE: GENRE 
Lecture 10: Frank Miller’s Dark Knight Returns 
a: Innovation and Originality 
b: The Rules of Genre 
c: Who is The Batman?

Lecture 11: Warren Ellis and John Cassady's Planetary 
a: The Possibilities of Genre 
b: The Limits of the Page 
Lecture 12: Neil Gaiman, Bill Willingham and the Vernacular Canon of Fantasy

WEEK SIX: MEDIA Lecture 13: Alan Moore and Dave Gibbons’ Watchmen 
a: The Media 
b: The Poetics of the Page 
c: Graphia

Lecture 14: Joe Sacco’s Palestine 
a: Faces Tell Stories 
b: Dead Ends

Lecture 15: Mark Millar and Bryan Hitch’s The Ultimates 
a: War Machine 
b: Star Power

WEEK SEVEN: CONCLUSION

Lecture 16: Chris Ware's Jimmy Corrigan and Building Stories 
a: The Aesthetics of Loneliness 
b: The Book of Honesty

Lecture 17: Comics Energy

Video 8: The Community of Comics (w/Chris Angel)





Recommended Background
No background reading in comics is required.  Comics are for everyone! 
Suggested Readings
In the spirit of a free course, I do not require you to buy books for this course.  The lectures and assignments are all designed to be self-contained.  Beyond the size of the class, this is the chief difference between the MOOC and the versions of the course I teach at the University of Colorado Boulder. 

I do encourage you to apply what you learn in the lectures to the comics you own, buy off the shelf of your local comic store, or borrow from your local library.  Reading as many comics as you can, as slowly and as carefully as you can, regardless of which ones, will make you a more sophisticated reader.

After week three, the lectures turn to specific texts.  If you are driven to buy some of the books, I would first suggest that you buy the following three:
1. Art Spiegelman's Maus (especially volume one)
2. Frank Miller's The Dark Knight Returns
3. Alan Moore and Dave Gibbons's Watchmen

If you still have some money burning a hole in your pocket, I would recommend these, in this order:
1. Alison Bechdel's Fun Home
2. Chris Ware's Building Stories
3. and finally, Warren Ellis and John Cassady's Planetary, and any book by David Mazzucchelli or Paul Pope.

The remaining books are: 
1. Neil Gaiman, et al., Sandman, Volume 3: "Dream Country."
2. Bill Willingham, et al., Fables, Volume 1: "Legends in Exile."
3. Joe Sacco, Palestine.
4. Mark Millar and Bryan Hitch, The Ultimates I & II.


Course Format
The class will consist of lecture videos and powerpoints, each between 10 and 25 minutes in length and followed by a brief quiz. There will also be two close reading papers, graded through peer-review, a comic shop project, and a comic book project.
FAQ
Will I get a Statement of Accomplishment after completing this class?
Yes. Students who successfully complete the class will receive a Statement of Accomplishment signed by the instructor.

What resources will I need for this class?

For this course, all you need is an internet connection and the time to view the videos, write the essays, and enjoy some marvelous works of art. You do not need to purchase the comics, as the lectures and assignments are self-contained; of course, you might enjoy reading and owning the comics.

Is this course suitable for children?

Comics are written by adults and are about themes that adults think about.  This course deals with these mature, often passionate and painful, themes.  It is not for children.

But aren’t comics really for illiterates?

Comics are a vibrant art form that spans every aspect of the humanities: literature and creative writing, art and design, world history, and studies of society, gender, race, and class.  If you haven’t read comics, or just haven’t read them in a long time, you are in for a profound experience.

Who would win in a fight, Superman or Dr. Manhattan?

This course isn’t really about questions like this, so much as big questions of art, history, and the human urge to create.  If you would like to know the answer to this question, draw your own comic!

Why should I bother with the essays?

Every writer can improve.  Writing essays will develop your writing skills and help you articulate your imagination.

What are three cool things I'll learn from this class?

You will learn a mode of reading that will allow you to appreciate comics in a new depth.  You will learn an effective mode of writing that will allow you to express your thinking clearly.  Most importantly, you will also learn the power of the imagination as a force for change: Art is Generative, so there is always hope.


Sessions

Join for Free
Course at a Glance
7 weeks
3-4 hours of work / week
English
English subtitles
Instructors

William Kuskin
University of Colorado Boulder
Categories
Humanities 
Arts
Share

Related Courses

Live!: A History of Art for Artists, Animators and Gamers

Fundamentals of Global Energy Business

Introduction to Clinical Neurology
Browse more courses
Coursera empowers people to improve their lives, the lives of their families, and the communities they live in with education.

© 2014 Coursera Inc. All rights reserved.

COMPANY

About
People
Leadership
Careers
FRIENDS

Partners
Community
Programs
CONNECT

Google+
Twitter
Facebook
Blog
Store
MORE

Terms
Privacy
Help
Press
Contact
