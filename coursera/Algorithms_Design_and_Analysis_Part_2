|Global Partners
CoursesSpecializationsNewInstitutionsAbout ▾ |Sign InSign Up

Algorithms: Design and Analysis, Part 2
In this course you will learn several fundamental principles of advanced algorithm design: greedy algorithms and applications; dynamic programming and applications; NP-completeness and what it means for the algorithm designer; the design and analysis of heuristics; and more.

Preview Lectures

Watch intro video
About the Course
In this course you will learn several fundamental principles of advanced algorithm design. You'll learn the greedy algorithm design paradigm, with applications to computing good network backbones (i.e., spanning trees) and good codes for data compression. You'll learn the tricky yet widely applicable dynamic programming algorithm design paradigm, with applications to routing in the Internet and sequencing genome fragments.  You’ll learn what NP-completeness and the famous “P vs. NP” problem mean for the algorithm designer.  Finally, we’ll study several strategies for dealing with hard (i.e., NP-complete problems), including the design and analysis of heuristics.  Learn how shortest-path algorithms from the 1950s (i.e., pre-ARPANET!) govern the way that your Internet traffic gets routed today; why efficient algorithms are fundamental to modern genomics; and how to make a million bucks in prize money by “just” solving a math problem!
Course Syllabus
Weeks 1 and 2: The greedy algorithm design paradigm.  Applications to optimal caching and scheduling.  Minimum spanning trees and applications to clustering.  The union-find data structure.  Optimal data compression.

Weeks 3 and 4: The dynamic programming design paradigm.  Applications to the knapsack problem, sequence alignment, shortest-path routing, and optimal search trees.

Weeks 5 and 6: Intractable problems and what to do about them.  NP-completeness and the P vs. NP question.  Solvable special cases. Heuristics with provable performance guarantees.  Local search. Exponential-time algorithms that beat brute-force search.
Recommended Background
How to program in at least one programming language (like C, Java, or Python); and familiarity with proofs, including proofs by induction and by contradiction.  At Stanford, a version of this course is taken by sophomore, junior, and senior-level computer science majors.  The course assumes familiarity with some of the topics from Algo 1 --- especially asymptotic analysis, basic data structures, and basic graph algorithms.

Suggested Readings
No specific textbook is required for the course.  Much of the course material is covered by the well-known textbooks on algorithms, and the student is encouraged to consult their favorite for additional information.


Course Format
The class will consist of lecture videos, generally between 10 and 15 minutes in length. These usually have integrated quiz questions. There will also be standalone homeworks and programming assignments that are not part of video lectures, and a final exam.
FAQ
Will I get a statement of accomplishment after completing this class? Yes. Students who successfully complete the class will receive a statement of accomplishment signed by the instructor.

How does Algorithms: Design and Analysis differ from the Princeton University algorithms course?
The two courses are complementary. That one emphasizes implementation and testing; this one focuses on algorithm design paradigms and relevant mathematical models for analysis. In a typical computer science curriculum, a course like this one is taken by juniors and seniors, and a course like that one is taken by first- and second-year students.


No sessions available
Add to Watchlist
Course at a Glance
6-10 hours of work / week
English
English subtitles
Instructors

Tim Roughgarden
Stanford University
Categories
Computer Science: Theory
Share

Related Courses

Fundamentals of Global Energy Business

Creative Problem Solving

中级有机化学 Comprehensive Organic Chemistry
Browse more courses
Coursera empowers people to improve their lives, the lives of their families, and the communities they live in with education.

© 2014 Coursera Inc. All rights reserved.

COMPANY

About
People
Leadership
Careers
FRIENDS

Partners
Community
Programs
CONNECT

Google+
Twitter
Facebook
Blog
Store
MORE

Terms
Privacy
Help
Press
Contact
