|Global Partners
CoursesSpecializationsNewInstitutionsAbout ▾ |Sign InSign Up

Music's Big Bang: The Genesis of Rock 'n' Roll
Explores the factors — musical and cultural — that led to the birth of American rock 'n' roll music in the early 1950s.


Watch intro video
About the Course
How did rock 'n' roll music come to be?

Join us to explore Music's Big Bang -- how music brought to America by immigrants evolved, blended and eventually became the delectable stew that is rock 'n' roll.
We will see and hear the ingredients in that stew, such as blues, jazz and gospel, and explore rock 'n' roll's first half decade, roughly 1955 to 1960. We will consider cultural factors, too, including the advance of technology, the migration of rural Americans from the South to the cities of the North, and the threats to mainstream American values presented by teenage rebellion and the Civil Rights Movement.

Course Syllabus
Week 1 What is rock 'n' roll? It's a whole lot more than just music. It's a business, an attitude, a cultural phenomenon, a showcase of technology and more.
Week 2 Before there was rock: Major musical genres, technological advances and players integral to the birth of rock 'n' roll. The genres include jazz, blues, country, western, bluegrass, gospel, popular music and more.
Week 3 Mississippi Ghosts: W.C. Handy and the Delta Blues of Robert Johnson, Son House, Charley Patton and Lead Belly.
Week 4 A Tale of Three Cities: New Orleans' jazz and early rock 'n roll, Chicago and its electric blues, and Memphis, Tenn., home of Elvis Presley and Sun Records.
Week 5 The emergence of Chuck Berry, Little Richard and Bo Diddley; the Sun Records "Class of 1955" and "The Day the Music Died."
Week 6 The major labels finally awaken and start to rock: The search is on for the next big thing, Elvis II.
Week 7 The five styles of rock 'n' roll as postulated by author Charlie Gillett.
Week 8 Summation and project review.

Recommended Background
No previous knowledge is required.
Suggested Readings
There are no required readings, but if you want to know more, there are dozens of great books you can buy. Here are a few recommendations:
Gillett, C. (1993). The Soul of the City: The Rise of Rock 'n' Roll (Second Edition). Cambridge, Ma: Da Capo Press.
Gioia, T. (2009). Delta Blues: The Life and Times of the Mississippi Masters Who Revolutionized American Music. New York, NY: W.W. Norton.
Palmer, R. (1982). Deep Blues: A Musical and Cultural History of the Mississippi Delta. New York, NY: Penguin.
Covach, J. & Flory, A. ((2012). What's That Sound? An Introduction to Rock and Its History (Third Edition). New York, NY: W.W. Norton.
Guralnick, P. (1995). Last Train to Memphis: The Rise of Elvis Presley. Boston, Ma.: Back Bay.
Mckeen, W. (2000). Rock 'n' Roll Is Here To Stay: An Anthology. New York, NY: W.W. Norton.
Guralnick, P. (1999). Sweet Soul Music: Rhythm and Blues and the Southern Dream of Freedom.  Boston, Ma.: Back Bay.
Course Format
The class will consist of lecture videos. Several videos, usually 8 to 12 minutes in length, will be posted each week. To advance through the course, you must complete a short weekly quiz by answering questions from the videos. 

An optional but exciting project involves creating your own interactive online timeline of events covered in our course.
FAQ
Will I get a Statement of Accomplishment after completing this class?
Yes. Students who successfully complete the class will receive a Statement of Accomplishment. Those who complete the course with distinction will receive a Statement of Accomplishment signed by the instructor. A verified certificate is available at extra cost for those who may want proof of completion for potential college credit.

What resources will I need for this class?
For this course, all you need is an Internet connection and the time to watch, listen, discuss, and enjoy some fabulous music.

What if I don't know anything about music? 

No problem. This is not a music course, and we will spend very little time talking about music theory, tempo or meter. This is about the cultural history of American music.
What is the coolest thing I'll learn if I take this class?
In addition to hearing and discussing some terrific music, this course aims to put history, technology and cultural events in perspective to help everyone better understand how rock 'n' roll music came to exist in America.

Sessions

Join for Free
Earn a Verified Certificate
Course at a Glance
8 weeks
4-6 hours of work / week
English
English subtitles
Earn official recognition for your work

Verified Certificate
Instructors

David Carlson
University of Florida
Categories
Music, Film, and Audio
Share

Related Courses

The Music of the Beatles

Age of Jefferson

History of Rock, Part Two
Browse more courses
Coursera empowers people to improve their lives, the lives of their families, and the communities they live in with education.

© 2014 Coursera Inc. All rights reserved.

COMPANY

About
People
Leadership
Careers
FRIENDS

Partners
Community
Programs
CONNECT

Google+
Twitter
Facebook
Blog
Store
MORE

Terms
Privacy
Help
Press
Contact
