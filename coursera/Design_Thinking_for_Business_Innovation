|Global Partners
CoursesSpecializationsNewInstitutionsAbout ▾ |Sign InSign Up

Design Thinking for Business Innovation
This course will provide an overview of the process and tools used for design thinking, and examine their application in organizational situations.


Watch intro video
About the Course
Design thinking is a popular new idea in the business world - organizations as diverse as entrepreneurial start-ups, big established corporations, and government and social service organizations are experimenting with design thinking as an alternative approach to traditional problem-solving. Accelerated by the spectacular rise of Apple and IDEO, design thinking is seen as offering a new approach better suited for dealing with the accelerating pressures for growth and innovation faced by so many organizations today. But design thinking can remain mysterious for people interested in introducing this approach into their decision-making processes. Demystifying it is the focus of this course.
Though designing as a craft requires years of dedicated education and talent to master, design thinking, as a problem solving approach, does not. In this course, we work with the following model that contains four questions and ten tools:

 
The four sequential questions that take us on a journey through an assessment of current reality (What is?), the envisioning of a new future (What if?), the development of some concepts for new-business opportunities (What wows?), and the testing of some of those in the marketplace (What works?). The process of design thinking begins with data gathering: at the outset of the design process, designers gather a great deal of data on the users they want to create value for.  They mostly do this through ethnographic methods like experience mapping, rather than traditional methods like focus groups and surveys. Farther along in the process, designers make their new ideas concrete (in the form of prototypes) and go out and get better data from the real world in a process that is hypothesis-driven. That is, they treat their new ideas as hypotheses to be tested. They surface the assumptions underlying their hypotheses and test them – usually looking for the kind of behavioral metrics that will allow them to iterate their way to improved value propositions.
Accompanying the four questions is a set of new tools to help business people achieve the same kind of disciplined approach to innovation and growth that they bring to the rest of their business. In this course, we will look at the stories of a wide variety of organizations - major corporations like IBM, entrepreneurial start-ups like MeYouHealth, and even social service organizations like The Good Kitchen - all using the design thinking tools and approach to achieve better outcomes.

Course Syllabus
Week 1: What is Design Thinking? We will begin our course by unpacking what we mean by design thinking and why it is more effective than traditional business methods when the goal is innovation in the business environment.  By looking at the case history of The Good Kitchen, a Denmark program for providing meals for the elderly, we will explore how the mindset and practice of the innovation team that partnered with innovation consultant Hatch & Bloom enabled them to achieve innovation and growth. We’ll conclude session 1 by examining what kinds of problems and challenges are best suited for a design thinking approach.
Week 2: How can we prepare ourselves to be leaders of innovation? Design thinking is not only about process and tools, however. It is about people as well - about you as a design thinker and about the people you want to create value for and with. And so, before we jump into the process of using design thinking to generate and test ideas, we want to first focus on your own mindset and look at how whether  your mind is prepared to both see and to act on opportunity when it shows up in your world. We will examine this issue by looking at the stories of two very capable managers George and Geoff - and how their differing mindsets impact their ability to lead innovation and growth.  We will also look at the role of visualization tools that use imagery and storytelling to bring ideas to life.

Week 3: How can you use design thinking to generate ideas? Now we will take a deeper dive into the design thinking process, looking at how we can use it to generate better ideas. This week, we will look at the story of an entrepreneur, Chris Cartter, and his start-up, MeYouHealth, as they worked with Boston design firm, Essential Design, to understand the kind of opportunity that social networking might hold for helping us to improve our health.  Examining what already exists is the first step in the design thinking process. As part of assessing what is designers “follow the customer home” and explore the problems they are trying to solve in life versus their product use. Once they have thoroughly explored and looked for patterns in what is, designers look towards the future and ask what if? These two questions - what is and what if -   along with the tools associated with them, will be the focus of week 3. This is the creative part of the process - but it also contains a disciplined approach. Ethnographic techniques like journey mapping help us dig deep to understand the whole customer experience. Tools like mind mapping help us make sense of the data collected. Tools like structured brainstorming and concept development help move from the creation of a mass of unrelated ideas to the construction of robust concepts that are worth moving into testing. 

Week 4: How can you use design thinking to test ideas?  Having generated all these great ideas, what’s next? The design thinking process now helps us to take the many ideas we have generated, and figure out how to figure out which ones are likely to produce the specific kind of outcomes we want -whether these take the form of improved nutrition for the elderly (The Good Kitchen), healthier life style choices (MeYouHealth) or even more “hot leads” emanating from your trade shows (in the IBM example we will look at in this session). In this session, we will follow the activities of an IBM team working closely with experience marketing agency, George P. Johnson, as they develop and test ideas for a revolutionary approach to trade show participation. This process begins by asking what wows? This question brings together the customer and business cases supporting our new concepts. Typically, the “wow” zone occurs at the intersection of three criteria: somebody wants it, we can create and deliver it, and doing so has the potential to produce the outcomes we as an organization want.  Then we ask  what works? and conduct small experiments to test out whether our assumptions are in fact accurate. Design thinking tools like  prototyping, co-creation and learning launch help us along the way.

Week 5: So what? Having now completed our review of the design thinking process, looked at some tools, and examined our own mindsets to be sure that we are prepared to lead, we turn to the question of outcomes. What kinds of changes can we expect to see coming out of the process? And moving forward, what do we need to do to capitalize on the opportunities that design thinking brings?

Recommended Background
Anyone interested in innovation in an organizational context would benefit from this class.
Suggested Readings
Designing for Growth: A manager’s design thinking toolkit, by Jeanne Liedtka and Tim Ogilvie. 
Solving Problems with Design Thinking: Ten Stories of What Works,  by Jeanne Liedtka, Andrew King and Kevin Bennett.  



Course Format
The course will consist of a series of videos and on-line discussions. 

STATEMENT OF ACCOMPLISHMENT

Students will be eligible to the State of Accomplishment if they have watched all the course videos, posted 4 times to the course forums, demonstrated the use of a minimum of 3 design tools (300 words or more) and posted the 3 tools usage on the forums. All the requirements must be met in order to achieve the Statement of Accomplishment, which is equivalent of earning 100% of the course points.
No sessions available
Add to Watchlist
Course at a Glance
2-4 hours of work / week
English
English subtitles
Instructors

Jeanne M. Liedtka
University of Virginia
Categories
Business & Management
Share

Related Courses

Age of Jefferson

On Strategy : What Managers Can Learn from Great Philosophers

Creativity, Innovation, and Change
Browse more courses
Coursera empowers people to improve their lives, the lives of their families, and the communities they live in with education.

© 2014 Coursera Inc. All rights reserved.

COMPANY

About
People
Leadership
Careers
FRIENDS

Partners
Community
Programs
CONNECT

Google+
Twitter
Facebook
Blog
Store
MORE

Terms
Privacy
Help
Press
Contact
