|Global Partners
CoursesSpecializationsNewInstitutionsAbout ▾ |Sign InSign Up

Understanding economic policymaking
This course will take a non-technical approach to understanding how governments influence the macroeconomy. Topics will include fiscal policy, deficits and debts, monetary policy and structural reform. We will review some current debates, such as fiscal stimulus vs. austerity and rules vs. quantitative easing.


Watch intro video
About the Course
Developed countries are immersed in the worst crisis since World War II.  How are governments responding?  Could they do more?  These are the questions that will be tackled in this course.

 

The course begins with a review of some basic macroeconomic concepts to lay the groundwork for the policy framework.  It then analyzes how fiscal policy works and what the effects of today´s outsized deficits and debts could be on national economies.  It then moves to monetary policy and explores its drawbacks and the recourse to other approaches, such as quantitative easing.  Finally, the course looks at the other policies open to governments to address a crisis, and the debate over how they have been used today.

 

Students will finish the course by trying their own hands at economic policymaking for an unidentified country, with an online simulator exercise.


Course Syllabus
Week One:

An overview of the macroeconomy and its basic variables: GDP, inflation, unemployment and the relationships among them.

Week Two:  

Diagnosing a macroeconomy.  What does the economy need as it passes through the different “gaps” in the business cycle?  Do any problems solve themselves?

Week Three:  

Fiscal policy as an expansive and restrictive policy tool.  How can governments use taxes and government spending to influence the economy?  What are the drawbacks of fiscal policymaking?  When are deficit spending and rising debt appropriate, what sizes give cause for concern, how can they be addressed? 

Week Four:  

Monetary policy as an expansive and restrictive policy tool. How does it work? What are its strengths and weaknesses?  What is the meaning of cyclical asymmetry and how have central banks addressed this problem in the crisis?  What might be the consequences?

Week Five: 
Combining policies.  How do different combinations of these policies affect the macroeconomy, in the short and the long run?

Week Six: 

The debate over rules vs. discretion in the wake of the financial crisis.  Trying your hand at economic policymaking.


Recommended Background
A basic introductory course in economics or business could help, but no knowledge is required; all are welcome.


Suggested Readings
The lectures are designed to be self-sufficient, but learning would be enhanced by the optional textbook (to be announced).


Course Format
The class will consist of lecture videos, which are each 20-30 minutes in length. These will be followed by optional, automatically graded quizzes to test student knowledge of the concepts discussed in the video and the readings. There will also be a final homework assignment and a mandatory final exam.


FAQ
Will I get a Statement of Accomplishment after completing this class?
Yes. Students who successfully complete the class will receive a Statement of Accomplishment signed by the instructor.

What resources will I need for this class?
For this course, all you need is an Internet connection, concentration and interest.  Use of the text will strengthen learning of the concepts.

What is the coolest thing I'll learn if I take this class?
This course will help you to be able to pick up a newspaper and understand what politicians and economists are saying on many very relevant issues.  It´s also fun to try a bit of policymaking on your own.

Sessions

Join for Free
Course at a Glance
6 weeks
3-5 hours of work / week
English
English subtitles
Instructors

Gayle Allard
IE Business School
Categories
Economics & Finance
Share

Related Courses

Fundamentals of Global Energy Business

Understanding Europe: Why It Matters and What It Can Offer You

On Strategy : What Managers Can Learn from Great Philosophers
Browse more courses
Coursera empowers people to improve their lives, the lives of their families, and the communities they live in with education.

© 2014 Coursera Inc. All rights reserved.

COMPANY

About
People
Leadership
Careers
FRIENDS

Partners
Community
Programs
CONNECT

Google+
Twitter
Facebook
Blog
Store
MORE

Terms
Privacy
Help
Press
Contact
