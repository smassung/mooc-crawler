|Global Partners
CoursesSpecializationsNewInstitutionsAbout ▾ |Sign InSign Up

Paradoxes of War
An introduction into the historical, psychological, and sociological analysis of organized conflict.


Watch intro video
About the Course
The Paradox of War teaches us to understand that war is not only a normal part of human existence, but is arguably one of the most important factors in making us who we are. Through this course, I hope that you will come to appreciate that war is both a natural expression of common human emotions and interactions and a constitutive part of how we cohere as groups. That is, war is paradoxically an expression of our basest animal nature and the exemplar of our most vaunted and valued civilized virtues. You will learn some basic military history and sociology in this course as a lens for the more important purpose of seeing the broader social themes and issues related to war. I want you to both learn about war, but more importantly, use it as way of understanding your everyday social world So, for example, the discussion of war and gender will serve to start you thinking about how expectations of masculinity are created and our discussion of nationalism will make clear how easy “us-them” dichotomies can be established and (ab)used. I will suggest some readings for you to complement the class and assign some activities through which you will be able to apply the theoretical insights from the course to your observations of everyday life. At the end of the course, you will start to see war everywhere and come to appreciate how much it defines our life.

Course Syllabus
The Warrior's War

Is War Natural?
Warriors in Battle
Why Not Run Away?
What Do Soldiers Believe In?
Brutality
Discipline
From Wars of Armies to Wars of Societies

Wars of Armies
Progress of Battle 
Gunpowder 
Industrialization of War
Technowar 
War and Society

Social Aspects of War
States
Nationalism
Soldiers and Citizens
War and Equality
Conquest. Genocide, and Armageddon
The Future of War

The Rise of the Rest
New Challenges
Conclusions: Empire and the Western Way of War
Recommended Background
No prior background is required.

Suggested Readings
Thucydides, The Peloponnesian War, Book 1, 2.1, 2.2-6, 2.7-24, 2.34-46, 2.47-54, 2.59-65, 2.71-78, 3.20-24, 3.35-50, 3.52-68, 3.81-84, 4.3-4.41, 4.46-484, 4.90-101, 4.117-11, 5.6-11, 5.14-24, 5.25-26, 5.42-48, 5.76-83, 5.85-116, Books 6 and 7
Homer, Iliad, Books 1,3, 7, 9, 24
Virgil Aeneid, Books 2, 4
Thomas E. Ricks, Making the Corps
E.B. Sledge, With the Old Breed,  Chaps 1-4, 10-15.
John Keegan, The Face of Battle, Chs. 2-4.
US Grant, Memoirs , Chs. 21-22, 24-25, 30-31, 33-37, 39, 42-44, 49-51, 55-57, 59, 62, 65, 67-68.
Michael Gordin, Red Cloud at Dawn, Chs. 1, 2,6, 7, Epilogue
Reviel Netz, Barbed Wire. Part III
Victor Davis Hanson, Carnage and Culture, Chs. 1, 6, 8, 10, Afterword.
Franz Fanon, The Wretched of the Earth, “Concernign Violence, Colonial War and Mental disorders, Conclusion.
Keith Lowe, Savage Continent, Part I (All), Chs. 8-10, 18-20, 22-24.
Ian Buruma, Year Zero
Course Format
This course will consist of lecture videos, activities and various reading assignments.  Students will also be encouraged to participate in the discussion forums.

FAQ
Does Princeton award credentials or reports regarding my work in this course?
No certificates, statements of accomplishment, or other credentials will be awarded in connection with this course.

Sessions

Join for Free
Course at a Glance
6 weeks
5-7 hours of work / week
English
English subtitles
Instructors

Miguel A. Centeno
Princeton University
Categories
Social Sciences
Share

Related Courses

Practical Ethics

Practicing Tolerance in a Religious Society: The Church and the Jews in Italy

Understanding Europe: Why It Matters and What It Can Offer You
Browse more courses
Coursera empowers people to improve their lives, the lives of their families, and the communities they live in with education.

© 2014 Coursera Inc. All rights reserved.

COMPANY

About
People
Leadership
Careers
FRIENDS

Partners
Community
Programs
CONNECT

Google+
Twitter
Facebook
Blog
Store
MORE

Terms
Privacy
Help
Press
Contact
