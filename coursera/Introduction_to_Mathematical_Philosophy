|Global Partners
CoursesSpecializationsNewInstitutionsAbout ▾ |Sign InSign Up

Introduction to Mathematical Philosophy
Learn how to apply mathematical methods to philosophical problems and questions.


Watch intro video
About the Course
Since antiquity, philosophers have questioned the foundations--the foundations of the physical world, of our everyday experience, of our scientific knowledge, and of culture and society. In recent years, more and more young philosophers have become convinced that, in order to understand these foundations, and thus to make progress in philosophy, the use of mathematical methods is of crucial importance. This is what our course will be concerned with: mathematical philosophy, that is, philosophy done with the help of mathematical methods.

As we will try to show, one can analyze philosophical concepts much more clearly in mathematical terms, one can derive philosophical conclusions from philosophical assumptions by mathematical proof, and one can build mathematical models in which we can study philosophical problems.

So, as Leibniz would have said: even in philosophy, calculemus. Let's calculate.

Course Syllabus
Week One: Infinity (Zeno's Paradox, Galileo's Paradox, very basic set theory, infinite sets).

Week Two: Truth (Tarski's theory of truth, recursive definitions, complete induction over sentences, Liar Paradox).

Week Three: Rational Belief (propositions as sets of possible worlds, rational all-or-nothing belief, rational degrees of belief, bets, Lottery Paradox).

Week Four: If-then (indicative vs subjunctive conditionals, conditionals in mathematics, conditional rational degrees of belief, beliefs in conditionals vs conditional beliefs).

Week Five: Confirmation (the underdetermination thesis, the Monty Hall Problem, Bayesian confirmation theory).

Week Six: Decision (decision making under risk, maximizing xpected utility, von Neumann Morgenstern axioms and representation theorem, Allais Paradox, Ellsberg Paradox).

Week Seven: Voting (Condorcet Paradox, Arrows Theorem, Condorcet Jury Theorem, Judgment Aggregation).

Week Eight: Quantum Logic and Probability (statistical correlations, the CHSH inequality, Boolean and non-Boolean algebras, violation of distributivity)
Recommended Background
We will not presuppose more than bits of high school mathematics.
Suggested Readings
We will give you lists of additional references later in the course.
Course Format
The class will consist of lecture videos, which are between 8 and 15 minutes in length. These contain 1-2 integrated quiz questions per video.
FAQ
Will I get a Statement of Accomplishment after completing this class?
Yes. Students who successfully complete the class will receive a Statement of Accomplishment signed by the instructors.

Sessions

Join for Free
Course at a Glance
10 weeks
2-3 hours of work / week
English
English subtitles
Instructors

Hannes Leitgeb
Ludwig-Maximilians-Universität München …
Stephan Hartmann
Ludwig-Maximilians-Universität München …
Categories
Mathematics
Humanities 
Share

Related Courses

Logic: Language and Information 1

Reason and Persuasion: Thinking Through Three Dialogues By Plato

Practical Ethics
Browse more courses
Coursera empowers people to improve their lives, the lives of their families, and the communities they live in with education.

© 2014 Coursera Inc. All rights reserved.

COMPANY

About
People
Leadership
Careers
FRIENDS

Partners
Community
Programs
CONNECT

Google+
Twitter
Facebook
Blog
Store
MORE

Terms
Privacy
Help
Press
Contact
