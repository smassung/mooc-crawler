|Global Partners
CoursesSpecializationsNewInstitutionsAbout ▾ |Sign InSign Up

Microeconomics Principles
Introduction to the functions of individual decision-makers, both consumers and producers, within the larger economic system. Primary emphasis on the nature and functions of product markets, the theory of the firm under varying conditions of competition and monopoly, and the role of government in promoting efficiency in the economy.


Watch intro video
About the Course
Why 3 Versions?
Note: In January 2014 we will offer 3 versions of this course so that you can choose a version that best meets your needs. Two of the versions are identical except for the duration of the course. The third version is both shorter and covers slightly less material, focusing on just the most critical aspects of microeconomics principles. See further below for more details about the differences.

About the Course
Most people make the incorrect assumption that economics is ONLY the study of money.  My primary goal in this course is to shatter this belief.  In fact, in the last 50 years economists have tackled some of the most interesting and important questions for humanity.  For instance, the following are just a few examples:

About Love and Marriage
Why is the divorce rate so high and what should we do in order to reduce it?
About the Environment
Why do we have so much pollution?
How much is an endangered species worth?
About Crime
Would legalizing marijuana lead to a reduction in crime?
About Labor Markets
Does increasing the Federal Minimum Wage put people out of work?
Why did so many women enter the labor force during the last 40 years?
About things you should be worrying about
Why is a college education a smart investment? Or isn't?
Why are the presidential candidates missing the point on college costs?
And about many other things:
Would school vouchers lead to improvements in public education?
Why a draft would only damage the army?
How to level the playing field in baseball?
AND, why is COURSERA offering this course free of charge?
Additional questions we will consider:

What economists do and the strange way they think? (Basic Concepts, Modeling and Trade)
What determines prices and quantity in a market economy? (Supply and Demand)
What is the relationship between free market economies and social welfare? (Market Efficiency and Government Intervention)
What are the distributional effects of taxes? (Elasticity and Taxes)
What is the relationship between production and costs in a firm? (Production and Costs)
How competitive firms choose their efficient level of output? (Competitive Output)
What are the pricing policies of firms with some degree of market power? (Firms With Market Power)
How much pollution is too much pollution? (Public Goods, Common Resources and Externalities)
We will be addressing many of these (and many more) questions during this course. Again, my main goal will to show you the way economists think and how to use this analytical system to answer questions related not only to these and other important human issues but pretty much to anything you end up doing with your life after this class. After all, as you will quickly find out, I believe everything is economics!

Course Syllabus

Week	Dates	4-week Format	8-week Format	16-week Format
1	January 20-January 26	Basic Concepts, Modeling and Trade
Supply and Demand	Basic Concepts, Modeling and Trade	Basic Concepts, Modeling and Trade
2	January 27-February 2	Market Efficiency and Government Intervention
Elasticity	Supply and Demand	Basic Concepts, Modeling and Trade, continued
3	February 3-February 9	Production and Costs
Competitive Output	Market Efficiency and Government Intervention	Supply and Demand
4	February 10-February 16	Firms With Market Power
Public Goods, Common Resources and Externalities	Elasticity and Taxes	Supply and Demand, continued
5	February 17-February 23	 	Production and Costs	Market Efficiency and Government Intervention
6	February 24-March 2	 	Competitive Output	Market Efficiency and Government Intervention
7	March 3-March 9	 	Firms With Market Power	Elasticity and Taxes
8	March 10-March 16	 	Public Goods, Common Resources and Externalities	Elasticity and Taxes
9	March 17-March 23	 	 	Production and Costs
10	March 24-March 30	 	 	Production and Costs
11	March 31-April 6	 	 	Competitive Output
12	April 7-April 13	 	 	Competitive Output
13	April 14-April 20	 	 	Firms With Market Power
14	April 21-April 27	 	 	Firms With Market Power
15	April 28-May 4	 	 	Public Goods, Common Resources and Externalities
16	May 5-May 11	 	 	Public Goods, Common Resources and Externalities, continued
Recommended Background
None--all are welcome!
Course Format
Each week of class consists of multiple 8-15 minute long lecture videos, integrated weekly quizzes, readings, an optional assignment and a discussion. Most weeks will also have a peer reviewed assignment, and there will be the opportunity to participate in a semester-long project. 

FAQ
Will I get a certificate for this course?
Yes. Students who successfully complete the class will receive a Statement of Accomplishment signed by the instructor.

What resources will I need for this class?
Got an up-to-date browser and a decent internet connection? You’re good to go!

How can I pass this class?
We want to give you multiple ways to pass, tailored to your educational needs. You can score well on the quizzes, participate substantially in the forums, or compile a project that encompasses the topics of the course. The choice is up to you!

Which course do I sign up for?
In January 2014, we offer you 3 versions of the course. There are 8- and 16-week versions which cover identical content, but at a different pace. The 4-week version is much more compressed and some topics had to be cut, but the main concepts are still covered. You are welcome to choose the version that best fits your need.

Sessions

Join for Free
Course at a Glance
4 weeks
4-12 hours of work / week
English
English, Chinese subtitles
Instructors

José J. Vázquez-Cognet
University of Illinois at Urbana-Champaign
Categories
Economics & Finance
Share

Related Courses

Financial Markets

Live!: A History of Art for Artists, Animators and Gamers

Principles of Macroeconomics
Browse more courses
Coursera empowers people to improve their lives, the lives of their families, and the communities they live in with education.

© 2014 Coursera Inc. All rights reserved.

COMPANY

About
People
Leadership
Careers
FRIENDS

Partners
Community
Programs
CONNECT

Google+
Twitter
Facebook
Blog
Store
MORE

Terms
Privacy
Help
Press
Contact
