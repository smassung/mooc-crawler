|Global Partners
CoursesSpecializationsNewInstitutionsAbout ▾ |Sign InSign Up

The Global Business of Sports
This course analyzes the business side of sports and discusses the intricacies of global sports leagues as well as various countries' sports strategies. You will be equipped with a framework and tools to understand and evaluate the business side of competitive sports around the world.


Watch intro video
About the Course
The wide range of global sports businesses constitutes a multi-billion dollar industry. This course will examine and explain the business of sports by analyzing the economic, legal, governance and success models in various sports industries. The primary focus will be on American team sports but comparative global models will be contemplated as well.  We will seek to find the answers to questions such as: what lessons can global sports entities learn from each other? What business strategies are used to retain the profitability of world leagues, sports teams and franchises? What lessons can be drawn from the United States based sports models? What revised models should new entrepreneurial leagues apply in order to achieve success? Substantive instruction will be drawn from the likes of the Olympics, FIFA World Cup, MLB, NFL, Premier League, various player unions, China, India, South Africa, Brazil and much more.   Ultimately, our goal is to equip the student with a framework and tools with which to understand and evaluate the business side of a wide range of competitive sports ventures.

Course Syllabus
Module One:  Sports Leagues and Teams I: Structure and Economics

Where is the Money? We will examine the structure of sports leagues and teams including a broad overview of the legal and financial structures. The lecture will also provide an introduction to the revenue generators and revenue distribution models so that students will be able to analyze a variety of sports business enterprises. For example, we will study U.S. and European league governance including promotion and relegation. Specifically, a case study of the NFL and Manchester United sponsorships will be used to engage the students in real time developments in the sports business industry. The important role of revenues from media rights fees will be introduced and more closely examined later as a primary financial driver. Other revenue generating streams such as ticket sales and sponsorships will be highlighted as well.

Module Two: Sports Leagues and Teams II: Globalization, Leadership and Ownership

This lecture will focus on the globalization of leagues, leadership and ownership. We will begin by looking at the popularity of leagues and begin a discussion on the largest sports leagues, teams and events. We will analyze the globalization of labor markets and revenue sources with the case of the Big 4 leagues in the U.S. Beyond globalization, we will examine leadership and ownership in the sports business industry. In addition, we will focus on the role of comissioners and analyze reasons for ownership as well as integration strategies that have been developed.

Module Three: Economic Drivers I: Media rights

This week will focus on the revenue sources that flow from the playing of sports events as well as ancillary activities. This includes the traditional radio and television sources as well as the ever growing and emerging new media sources. We will begin by taking a step back to list some of the first ever live televised sporting events including the 1927 Arsenal vs Sheffield game at Highbury and the1939 baseball game between Princeton and Columbia. We will also pay special attention to the rapid growth of media rights fees and include examples such as the NBC Olympics deal. Beyond the revenues that flow from these sources we will also examine how the use of and partnering with these media outlets have evolved from an initial fear of cannibalizing the live gate to current issues about the delivery of "free" content and the impact on the ever-changing sports business model. This will include the increasing popularity of Regional Sports Networks.

Module Four: Economic Drivers II: Stadiums and Arenas

The focus here will be on these facilities as both revenue and economic impact generating facilities. Is there a formula used to plan for the development of sports infrastructure? We will focus on stadiums and arenas and delve into the history of the construction of great sports centers and their impact. The case of AEG as a sports facility owner will be examined as well as specific development sagas in Los Angeles and Philadelphia..

Module Five: Player Salaries, Unions and Sports Agents

The largest figures on the expense side of the sports business ledger are player salaries. This lectuure focusses on salaries as well as the key proponents of those salary increases. Sports agents and unions are key stakeholders in the sports business industry. Agents source and manage talent while unions use collective bargaining to champion for an organization of the economic system that favors salary increases. We will analyze the impact these stakeholders have had on the industry in different regions in the world and use case examples including lockouts from the past. Students will have the opportunity to compare and contrast unions around the world.

Module Six: The Olympics and The FIFA World Cup

We will take a close look at the Olympics history and economic impact. What goes into planning and executing the Olympics? We will analyze the 4-year cycle that businesspersons and athletes take to prepare for the Olympics. We will also examine ideas about the economic impact of these games on the host country’s and the world’s economy. Similarly, the FIFA World Cup, by many measures, is the world’s biggest sports event.  We will discuss the role of World Cup plays on the world’s business environment using the following lenses to analyze the tournament: fans, countries, players, media, host country, retailers and of course FIFA.

Module Seven: Sports Enterprise Global Strategies

How do sports entity leaders develop and execute global strategy? We will conclude by contrasting the different penetration levels of various sports and their presence on the global platform. We will use the framework we have developed to hypothesize the globalization of various sports and examine their financial and operational feasibility.  We’ll contemplate the elements needed for a truly global, profitable sport entity. 

Recommended Background
None; all are welcome.

Suggested Readings
You do not need to purchase anything. Although the lectures are designed to be self-contained, supplemental readings will be recommended throughout the course for those who wish to delve further into particular topics. Students wishing to fully engage with the course content should read my book, The Business of Sports, by Scott Rosner and Kenneth L. Shropshire, (2d. edition, Jones & Bartlett Learning, 2011) as will be set forth in the course syllabus.

Course Format
The course will consist of short weekly lectures associated with quizzes, exercises, case studies and a discussion forum. There will be an optional final exam delivered for those wanting to complete the Statement of Accomplishment for this course. In the lecture segments, I’ll describe different sports business concepts and illustrate them with examples and diagrams. I’ll often use real-world case studies as illustrations to make the ideas easily retainable. In a few of the segments, I will interview leading sports industry stakeholders and provide links to relevant web or podcasts.

FAQ
Will I get a Statement of Accomplishment after completing this class?
Yes as long as you opt to take the final exam to ascertain that you have retained course concepts. No Statements of Accomplishment or other credentials will be awarded otherwise in connection with this course.

 Do I earn University of Pennsylvania credits upon completion of the course?
No. However, students who successfully complete the course above a threshold score will receive a Statement of Accomplishment signed by the instructor.

What resources will I need for this class?
There are no requirements or prerequisites for the course. Students wishing to fully engage should read, The Business of Sports, by Scott Rosner and Kenneth L. Shropshire, as will be set forth in the course syllabus.

What are the learning outcomes of this course and why should I take it?
By the end of the course you will have a better understanding of the business of sports, sports enterprise structures and the interests of various stakeholders in the sports business industry.



For more information on Penn’s Open Learning initiative, please go to:
http://provost.upenn.edu/initiatives/openlearning



No sessions available
Add to Watchlist
Course at a Glance
4-6 hours of work / week
English
English subtitles
Instructors

Kenneth L. Shropshire
University of Pennsylvania
Categories
Business & Management
Social Sciences
Share

Related Courses

Sports and Society

Exercise Physiology: Understanding the Athlete Within

New Models of Business in Society
Browse more courses
Coursera empowers people to improve their lives, the lives of their families, and the communities they live in with education.

© 2014 Coursera Inc. All rights reserved.

COMPANY

About
People
Leadership
Careers
FRIENDS

Partners
Community
Programs
CONNECT

Google+
Twitter
Facebook
Blog
Store
MORE

Terms
Privacy
Help
Press
Contact
