|Global Partners
CoursesSpecializationsNewInstitutionsAbout ▾ |Sign InSign Up

Sports and Society
This course explores the role of sports around the world, and how the games we watch and play shape identity, culture, and society.


Watch intro video
About the Course
Sports play a giant role in contemporary society worldwide. But few of us pause to think about the larger questions of money, politics, race, sex, culture, and commercialization that surround sports everywhere. This course draws on the tools of anthropology, sociology, history, and other disciplines to give you new perspectives on the games we watch and play. We will focus on both popular sports like soccer (or “football,” as anyone outside America calls it), basketball, and baseball, and also lesser-known ones like mountain-climbing and fishing. Special guests will include former major league baseball player and ESPN commentator Doug Glanville; leading sports journalist Selena Roberts; best-selling soccer author David Goldblatt; skateboarding expert Dwayne Dixon; and Katya Wesolowski, a specialist in the Afro-Brazilian martial art of capoeira. You will never watch or think about sports in the same way again.
Course Syllabus
Week One: Introduction to key concepts in sports studies, including the distinctions between play, games, and sports; examination of the 19th century rise of organized professional sports.  Case study focus on the Afro-Brazilian martial art of capoeira.

Week Two: exploration of the globalization of sports, and the relationship between sports and politics, nationalism, and social protest.  Case study focus on the rise of South American soccer, originally imported from England in the 19th century; Hitler’s 1936 Berlin Olympics; and the Black Power Protest at the 1968 Mexico City Olympics.

Week Three: race and race relations in sports, including the history of African Americans in sports and new Asian sports stars.  Case study of golf superstar Tiger Woods.

Week Four: gender and sexuality; how sports have been linked to idea of male prowess, and, more recently, the rise of women’s sports; the question of homophobia and its continuing presence in the sports world.  Case study focus on women’s figure-skating and the American NFL.

Week Five: sports fans and sports performance. Examines the question of why so many of us spend so much time watching and going to games, including the role of aesthetics and beauty in sports. Case study focus on the debate about big-time American college sports. 

Week Six: the business of sports; considers the enormous growth of the multibillion dollar business of sports, including video games, apparel, licensing, and other forces.  Case study focus on sports apparel megacompany Nike and the vast new popularity of sports videogames.

Week Seven: the growth of extreme sports, and larger questions about the relation between sports and the purpose and meaning of life.  Case study focus on mountain-climbing, fishing, and skateboarding.     
Recommended Background
All welcome. No previous knowledge required.
Suggested Readings
All suggested books are optional. Links to Amazon.com are to provide students with information about the books, not to suggest that Amazon.com is the recommended retailer. 

Eduardo Galeano, Soccer in Sun and Shadow 
Doug Glanville, The Game From Where I Stand 
Orin Starn, The Passion of Tiger Woods
Course Format
The class consists of lecture videos, mostly between 10 and 18 minutes in length. There will also be optional Google Hangouts with guest lecturers, which will give students the opportunity to interact with Professor Starn and influential figures from the world of sports and sports studies.  Student time each week will consist of watching about two hours of lecture; a required weekly reading assignment; a homework assignment on each reading; and a weekly quiz. There will also be an optional "Sports Movie of the Week," which we will discuss in a special section in the online forum of the class.
FAQ
Q: Will I get a Statement of Accomplishment after completing this class?

A: Yes. Students who successfully complete the class will receive a Statement of Accomplishment signed by the instructor. 

Q: I already know a lot about sports.  Will I really learn anything new?

A: Many students who take my classes at Duke are rabid sports fans.  But they find that this course gives them a much deeper understanding of the intriguing questions of politics, society, culture, globalization and much more that surround sports.   

Q: Who should take this class?

A: You’ll find the course useful whether you are thinking about sports as a profession (for example, as an agent, athlete, or coach) or just want to understand more about them and their very large role in our world today.

Q: Sports seems like a frivolous topic.  Why should I waste my valuable time on this course?

A: Sports have become such a major part of global society that scholars have been turning more attention to trying to understand them.   There are many exciting new concepts and insights in the growing field of sports studies that we will explore in the class.

Q: What’s an odd stray bit of information I’ll learn in this class?

A: You’ll find out how superstar golfer Tiger Woods became a national celebrity at the age of two.
Sessions

Join for Free
Course at a Glance
7 weeks
3-5 hours of work / week
English
English subtitles
Instructors

Orin Starn
Duke University
Categories
Humanities 
Social Sciences
Share

Related Courses

Latin American Migration

Age of Jefferson

Student Thinking at the Core
Browse more courses
Coursera empowers people to improve their lives, the lives of their families, and the communities they live in with education.

© 2014 Coursera Inc. All rights reserved.

COMPANY

About
People
Leadership
Careers
FRIENDS

Partners
Community
Programs
CONNECT

Google+
Twitter
Facebook
Blog
Store
MORE

Terms
Privacy
Help
Press
Contact
