|Global Partners
CoursesSpecializationsNewInstitutionsAbout ▾ |Sign InSign Up

First-Year Composition 2.0
First-Year Composition 2.0 will help you to develop a better process and gain confidence in written, visual, and oral communication and to create and critique college-level documents and presentations.


Watch intro video
About the Course
First-Year Composition 2.0 will help you to develop a better process and gain confidence in written, visual, and oral communication and to create and critique documents and presentations in college, in the workplace, and in your community. You will draft and revise the following assignments: a personal essay, an image, and an oral presentation. 

You will develop confidence in the following areas: 
Critical Thinking: Evaluate the effectiveness of personal essays, images, and oral presentations. Assess your work and the work of your peers. Reflect on your own processes and performance. 
Rhetoric: Analyze the ways in which you and other communicators use persuasion. Think about and use context, audience, purpose, argument, genre, organization, design, visuals, and conventions. 
Process: Apply processes (read, invent, plan, draft, design, rehearse, revise, publish, present, and critique). 
Digital Media: Produce written, oral, and visual artifacts. 
Course Syllabus
Course Outline 
Week One: Establishing Concepts, Practices, and Learning Goals 
Assignments: Self- Assessment Surveys & Personal Benchmark Statement

Weeks Two & Three: Written Communication
Major Assignment – Personal Philosophy Essay

Weeks Four & Five: Visual Communication 
Major Assignment – Personal Philosophy Visual 
  
Weeks Six & Seven:  Oral 
Major Assignment – Personal Philosophy Recorded Presentation 

Week Eight: Reflection  
Assignment – Re-visit the Self-Assessment Surveys and Personal Benchmark Statement
Recommended Background
Fluent English language literacy as well as grammatical and mechanical knowledge is required. You will need familiarity with a basic word-processing program (for example, Word, OpenOffice, Google Docs, Pages), basic image software (for example, PowerPoint, iPhoto, Photoshop, Picasa) or cameras (for example, smartphones, digital cameras), and basic audio recording software (for example, Audacity, GarageBand).
Suggested Readings
See course syllabus for required and suggested readings. The final syllabus will be available two weeks before the course begins.
Course Format
This class consists of two short introductory videos (a welcome video and a technology video), 24 ten-minute videos (three videos per week), plus approximately eight 20-to-30-minute recorded "Hangout" discussion sessions. These videos are complemented by additional written, oral, and visual materials; student activities; and web-based assessments.
FAQ
What kind of time will I need to take this course? You should plan 5-7 hours per week, but not in one block. We recommend that you plan 30-to-60-minute blocks, so that you have time to reflect between sessions when you’re working on a text, visual, or oral presentation. 

What are the most useful things I'll learn if I take this class? You’ll develop a stronger sense of process (how to approach and complete tasks), leading to increased confidence in completing and assessing written, visual, and oral communication projects. 

What resources will I need for this class? Beyond a positive attitude, time to do the readings and assignments, and a willingness to work, you will need regular access to a computer, a connection to the Internet, and basic software for doing written, visual, and oral activities. While you can use commercial software if you already own it, all of the software you need is free and available online (a list of some software resources is on the syllabus). You do not need to buy a textbook; instead, you will be referred to free, online texts—Writing Commons and the PurdueOWL. 

Will I receive transferable college course credit? This course is intended to introduce you to principles and practices of first-year composition. It is not intended as a substitute for a for-credit composition course at any college or university. Even though this course is not intended to be given college credit, it can demonstrate that you have learned a great deal. To keep a record of your accomplishments in this course, you should create a portfolio of your written, visual, and oral assignments. At some colleges and universities, your portfolio may form a portion of a case you might make for potential transfer credit. Every college has its own policy for acceptance of transfer or examination credit. 

Will I get feedback on my written work? Yes. You will receive peer assessments of your work. In addition, Professor Head and her teaching team will model effective feedback practices so class members can respond productively to one another. Most important for developing competence and confidence, you will learn how to assess your own work. We will also hold weekly virtual discussions and use selected projects for examples (anonymously and with your permission). 

Will this course provide instruction on grammar? No. Mechanics (for example, punctuation, capitalization, spelling) and grammar are important, so you’ll be given references to check mechanics and grammar. However, this course focuses on creating effective writing, visuals, and oral presentations. 

Is the course appropriate if a student is not very proficient in the English language? This course is about composing in English, so all course work must be completed in English. You will need English fluency to be successful in the course. Because we welcome cultural and linguistic diversity, part of our discussions will focus on the ways different people use language and the different expectations people bring to communication.
No sessions available
Add to Watchlist
Course at a Glance
5-7 hours of work / week
English
English subtitles
Instructors

Karen Head
Georgia Institute of Technology
Categories
Humanities 
Share

Related Courses

Applying to U.S. Universities

Writing II: Rhetorical Composing

Crafting an Effective Writer: Tools of the Trade
Browse more courses
Coursera empowers people to improve their lives, the lives of their families, and the communities they live in with education.

© 2014 Coursera Inc. All rights reserved.

COMPANY

About
People
Leadership
Careers
FRIENDS

Partners
Community
Programs
CONNECT

Google+
Twitter
Facebook
Blog
Store
MORE

Terms
Privacy
Help
Press
Contact
