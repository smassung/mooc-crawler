|Global Partners
CoursesSpecializationsNewInstitutionsAbout ▾ |Sign InSign Up

Case-Based Introduction to Biostatistics
Learn to frame and address health-related questions using modern biostatistics ideas and methods.


Watch intro video
About the Course
The course objective is to enable each student to enhance his or her quantitative scientific reasoning about problems related to human health. Biostatistics is about quantitative approaches - ideas and skills - to address bioscience and health problems. To achieve mastery of biostatistics skills, a student must “see one, do one, teach one.” Therefore, the course is organized to promote regular practice of new ideas and methods. 

The course is organized into 3 self-contained modules. Each module except the first is built around an important health problem. The first module reviews the scientific method and the role of experimentation and observation to generate data, or evidence, relevant to selecting among competing hypotheses about the natural world. Bayes theorem is used to quantify the concept of evidence. Then, we will discuss what is meant by the notion of “cause.” 
  
In the second module, we use a national survey dataset to estimate the costs of smoking and smoking-caused disease in American society. The concepts of point and interval estimation are introduced. Students will master the use of confidence intervals to draw inferences about population means and differences of means. They will use stratification and weighted averages to compare subgroups that are otherwise similar in an attempt to estimate the effects of smoking and smoking-caused diseases on medical expenditures. 

In the final module, we will study what factors influence child-survival in Nepal using data from the Nepal Nutritional Intervention Study Sarlahi or NNIPPS. Students will estimate and obtain confidence intervals for infant survival rates, relative rates and odds ratios within strata defined by gestational period, singleton vs twin births, and parental characteristics.
Recommended Background
Interest in the scientific method as broadly related to human health. Ability to reason precisely. Mathematics through pre-calculus.
Suggested Readings
The Mismeasure of Man by Stephen J. Gould is an outstanding resource, but it is not a required text for this course.
Course Format
For each module, students will complete a set of practice problems and then one or two graded quizzes to demonstrate their mastery of the ideas and methods taught in the module. Material will be introduced via 10-15 minute videos. Self-evaluation questions will be available for students to test their understanding of the material. Students will be encouraged to participate in on-line discussions to answer questions about the main ideas and methods.
FAQ
Will I earn a Statement of Accomplishment after completing this course?
Students who achieve a sufficient grade will receive a statement of accomplishment signed by the course instructor. 

Will I be able to participate if I don't speak English?
All lectures and course communications will be delivered in English. You are welcome to communicate with classmates in the language of your choice, but all course assignments must be submitted in English. 
Sessions

Join for Free
Course at a Glance
6 weeks
4-6 hours of work / week
English
English subtitles
Instructors

Scott L. Zeger
Johns Hopkins University
Categories
Health & Society
Statistics and Data Analysis
Share

Related Courses

Design and Interpretation of Clinical Trials

Epidemiology: The Basic Science of Public Health

Statistical Analysis of fMRI Data
Browse more courses
Coursera empowers people to improve their lives, the lives of their families, and the communities they live in with education.

© 2014 Coursera Inc. All rights reserved.

COMPANY

About
People
Leadership
Careers
FRIENDS

Partners
Community
Programs
CONNECT

Google+
Twitter
Facebook
Blog
Store
MORE

Terms
Privacy
Help
Press
Contact
