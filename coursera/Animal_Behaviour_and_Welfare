|Global Partners
CoursesSpecializationsNewInstitutionsAbout ▾ |Sign InSign Up

Animal Behaviour and Welfare
Animal welfare has been described as a complex, multi-faceted public policy issue which includes important scientific, ethical, and other dimensions. Improving our understanding of animal welfare, involves the fascinating study of animal behavior as well as the challenge of accessing the emotions of animals.


Watch intro video
About the Course
Animals occupy a huge part of the planet and our lives, and although we rely on them for all aspects of our own wellbeing - food, draught power, medical advances, clothing, sport as well as pleasure, protection and comfort - often their quality of life is questionable. Appreciating how animal's experience the world they live in and the different behavioural needs of the various species we interact with, enables us to gain a better understanding of their welfare requirements, so that long term improvements to animal lives can be made.  

There are now more than 60 billion land animals raised for meat each year around the world, and with increasing human populations and a rise in meat consumption in many parts of the world, these figures are set to double by 2050. Added to this is a huge and growing world population of dogs and cats, many of whom are strays with associated health and welfare issues.  International concern for animal welfare continues to grow with rising demand for measures to protect animals and improve their care and wellbeing. The link between animal welfare and human wellbeing is clear, and yet we still have a long way to go if we are to address welfare needs globally. Finding ways to achieve higher standards of animal welfare, is therefore a key priority for any developed and developing nation. Due to gaining in importance internationally, there is increasing recognition of the need for animal welfare issues to be addressed objectively in a scientifically credible manner. 

In this animal behaviour and welfare course, you will learn about animal welfare and why it matters, develop an understanding of some of the main welfare issues animals have to cope with as well as gaining an insight into the behavioural needs and the emotions of dogs, cats, farmed animals and captive wildlife.


This course is delivered collaboratively by academics from the University of Edinburgh and Scotland's Rural College (SRUC).

  

Course Syllabus
Week 1:  Introduction, History, and Concepts of Animal Welfare.

During this week, we will consider what animal welfare is, the meanings and definitions of animal welfare and the history of animal welfare in different parts of the world and how views on animal welfare are influenced by cultural, social, economical, political and other factors.

 

Week 2: Animal Behaviour and Animal Welfare Assessment

Animals show both short-term and long-term behavioural, physiological responses and immunological responses to challenges in their environment. We can use knowledge of animal physiology and both normal and abnormal behaviour as complementary approaches in the assessment of the welfare of an animal in its captive environment. We can also employ experimental techniques to try to understand animal emotions in relation to their living conditions, for example; to determine their preferences, or what they don't like or find aversive about a  procedure or environment. These experimental techniques allow us to ‘ask’ the animals how they feel about their environment. In week 2 we will explore a variety of techniques used practically and experimentally to provide an insight into the animal’s experience of its world.

 

Week 3: Practical Animal Welfare: Companion Animal Welfare

In many countries dogs and cats are traditionally kept as pets in the home, with stray/abandoned or unwanted animals usually housed in shelters, where there are frequently questions about their behavioural needs. In other parts of the world dogs and cats may exist very successfully as part of a ‘community’ or as ‘street’ animals, and this existence also presents unique challenges and benefits to the welfare of these species. In this lecture we will explore the place of dogs and cats in human society and how different attitudes and management styles impact on their welfare.

 

Week 4: Practical Animal Welfare: Production Animal Welfare

Production animal welfare is a hot topic. In this session you will gain an appreciation of the relevance of farm animal welfare and the issues that impact on production animal species.  We shall also look at the way that scientists have contributed to our understanding of the importance of these impacts from an animal's point of view. We will explore societal concerns, the scientific understanding of animal welfare within a variety of production methods and how we gain insight to the animal's viewpoint. 

 

Week 5: Practical Animal Welfare: Captive Wild Animal Welfare

During the fifth week we will summarise key welfare issues relating to the management of captive wild animals in zoos. Primarily conservation-focussed, the zoo community is becoming increasingly aware of the welfare concerns arising from the management of captive wild animals and is developing strategies to address these issues. We will look at the conflicts between welfare and conservation, the methods for assessing welfare within a zoo environment and the challenges associated with this.



Recommended Background
  No background is required.


Suggested Readings
There are many books on Animal Behaviour and Welfare. These books are not required for the course and they are not used specifically with the lectures. These are just some of our suggestions for books that you might find useful:



Appleby, M.C. (1999), What should we do about animal welfare?, Blackwell Science, Oxford

Dawkins, M S (1980), Animal Suffering: The Science of Animal Welfare, Chapman & Hall, London

Mason, G., Rushen, J. (2006) (2nd Ed), Stereotypic Animal Behaviour: fundamentals and applications to welfare . CABI publishing

Gregory, N. G. (2004) Physiology and behaviour of animal suffering. Blackwell Publishing

Grandin, T. (2000)(2nd Ed.), Livestock Handling and Transport, CAB International

Gregory, N.G. (2007), Animal welfare and meat production, CABI Publishing

Tudge, C. (1991), Last Animals at the Zoo, Oxford University Press



FAQ
Will I get a certificate after completing this class?
Yes. Students who successfully complete the class will receive a Statement of Accomplishment signed by the instructor.

Do I earn University of Edinburgh credits upon completion of this class?
No. The Statement of Accomplishment is not part of a formal qualification from the University. However, it may be useful to demonstrate prior learning and interest in your subject to a higher education institution or potential employer.

What resources will I need for this class?
No resources needed.

What are the learning outcomes of this course and why should I take it?
You will learn about; Animal welfare and why it matters and how we currently assess welfare plus gain an insight into the fascinating world of dogs, cats, farmed animals and captive wildlife.



Sessions

Join for Free
Course at a Glance
5 weeks
1-3 hours of work / week
English
English subtitles
Instructors

Natalie Waran
The University of Edinburgh
Fritha Langford
The University of Edinburgh
Heather J Bacon
The University of Edinburgh
Categories
Biology & Life Sciences
Share

Related Courses

Practical Ethics

Latin American Migration

Fundamentals of Global Energy Business
Browse more courses
Coursera empowers people to improve their lives, the lives of their families, and the communities they live in with education.

© 2014 Coursera Inc. All rights reserved.

COMPANY

About
People
Leadership
Careers
FRIENDS

Partners
Community
Programs
CONNECT

Google+
Twitter
Facebook
Blog
Store
MORE

Terms
Privacy
Help
Press
Contact
