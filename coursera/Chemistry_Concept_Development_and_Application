|Global Partners
CoursesSpecializationsNewInstitutionsAbout ▾ |Sign InSign Up

Chemistry: Concept Development and Application
This introduction to fundamental chemical concepts of atomic and molecular structure will emphasize the development of these concepts from experimental observations and scientific reasoning.


Watch intro video
About the Course
This course will cover an introduction to the atomic molecular structure of matter, similar to a typical first semester General Chemistry course.  The fundamental concepts will be introduced via the Concept Development Approach developed at Rice University and utilizing a free on-line textbook, Concept Development Studies in Chemistry, available via Rice’s Connexions project.  In this approach, rather than simply telling you the concepts you need to know and then asking you to memorize them or apply them, we will develop these concepts from experimental observations and scientific reasoning.  There are several reasons for using this approach.  One reason is that most of us our inductive learners, meaning that we like to make specific observations and then generalize from there.  Many of the most significant concepts in Chemistry are counter-intuitive.  When we see where those concepts come from, we can more readily accept them, explain them, and apply them. A second reason is that scientific reasoning in general and Chemistry reasoning in particular are inductive processes.  This Concept Development approach illustrates those reasoning processes.  A third reason is that this is simply more interesting.  The structure and reactions of matter are fascinating puzzles to be solved by observation and reasoning.  It is more fun intellectually when we can solve those puzzles together, rather than simply have the answers to the riddles revealed at the outset.
Course Syllabus
Course Outline
The Atomic Molecular Theory
Atomic Masses and Molecular Formulae
The Structure of an Atom
The Electron Shell Model of an Atom
Quantum Electron Energy Levels in an Atom
Electron Orbitals and Electron Configurations in Atoms
Covalent Bonding and Electron Pair Sharing
Molecular Structures
Energy and Polarity of Covalent Chemical Bonds
Bonding in Metals and Metal-Non-Metals Salts
Molecular Geometry
Measuring Energy Changes in Chemical Reactions
Reaction Energy and Bond Energy
Recommended Background
The class can be taken by someone with no prior experience in chemistry.  However, some prior familiarity with the basics of chemistry is desirable as we will cover some elements only briefly.  For example, a prior high school chemistry class would be helpful.
Suggested Readings
Readings will be assigned from the on-line textbook “Concept Development Studies in Chemistry”, available via Rice’s Connexions project.  In addition, we will suggest readings from any of the standard textbooks in General Chemistry.
Course Format
Lectures:  approximately 2 hours of lecture will be given each week in 15-20 segments.

Reading will be assigned for each topic from Concept Development Studies in Chemistry. Additional readings will be recommended from a second General Chemistry text of your choosing. 

An online quiz will be associated with each topic. A final exam will cover all topics, and online homework will be assigned weekly.
FAQ
Will I get a certificate after completing this class?
Rice University does not award certificates or issue transcripts or other credentials for student work in this course. However, Coursera will maintain limited data regarding student progress and performance in this course and, with your permission, provide authorized third parties with access to such data.

Will I know the basics of Chemistry if I complete this class?
Yes, assuming you have done all the reading, watched all of the lectures, and successfully completed all assignments.

Does this cover an entire first year of Chemistry?
No, this class covers the first semester of a standard two semester sequence. The second semester will be made available at a later date, assuming sufficient interest.

Sessions

Join for Free
Course at a Glance
10 weeks
4-6 hours of work / week
English
English subtitles
Instructors

John Steven Hutchinson
Rice University
Categories
Chemistry
Share

Related Courses

中级有机化学 Comprehensive Organic Chemistry

Practical Ethics

Beauty, Form & Function: An Exploration of Symmetry
Browse more courses
Coursera empowers people to improve their lives, the lives of their families, and the communities they live in with education.

© 2014 Coursera Inc. All rights reserved.

COMPANY

About
People
Leadership
Careers
FRIENDS

Partners
Community
Programs
CONNECT

Google+
Twitter
Facebook
Blog
Store
MORE

Terms
Privacy
Help
Press
Contact
