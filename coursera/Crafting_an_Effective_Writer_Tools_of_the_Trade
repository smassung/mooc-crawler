|Global Partners
CoursesSpecializationsNewInstitutionsAbout ▾ |Sign InSign Up

Crafting an Effective Writer: Tools of the Trade
Learn to become an effective builder of sentences using the basic tools of grammar, punctuation, and writing. Next Session: February 7, 2014


Watch intro video
About the Course
Learn to become an effective builder of sentences using the basic tools of grammar, punctuation, and writing. By dedicating yourself to the craft of writing, you will learn to use the eight parts of speech and grammar to develop the four basic sentence types into a well-organized, detailed paragraph. This course is designed for anyone who wants to become a better writer. If you need to write more clearly for work, prepare for a placement test for a college, or improve your skills for current writing projects, this class will definitely be beneficial. Please be aware that basic writers are the targeted audience for this course. 

Objectives: 
Students will be able to identify and correct some sentence level grammatical and punctuation errors.
Students will be able to develop four sentence types: simple, compound, complex and compound-complex.
Students will be able to recognize and use all four sentence types.
Students will be able to develop a clear topic sentence.
Students will be able to write a well-organized, detailed paragraph.
Why would you want to take this course?
As a pre-Assessment activity
As a petition for English placement
As a brush-up for students while in writing classes
As a brush-up for students who don’t/can’t enroll in an English class
As a resource for flipped/blended classrooms
To become a better writer 

Certified Course
    

Crafting an Effective Writer by Mt. San Jacinto College is licensed under a Creative Commons Attribution-NonCommercial 3.0 Unported License.
Course Syllabus
Unit 1:  Becoming a Successful Online Learner 
Introduction to the course with a course navigation tutorial; strategies for being successful in the course and in any online learning environment; information about how you might make your success in the course useful in your future educational experience. 
Unit 2:  Parts of Speech 
The eight main parts of speech; why knowing the parts of speech will help your writing; the sentence; observing the world.
Unit 3:  Subjects and Verbs 
Another way of looking at the parts of speech -- the building materials put to use in the structure of the sentence; gerunds, imperatives, and infinitives; thinking about verb tense and voice; regular and irregular verbs; building your confidence with empowered sentences. 
Unit 4:  Clauses and Phrases 
Adding to your basic sentences with clauses and phrases; combining ideas and creating complexity; the four sentence types; creating your writing style. 
Units 5 and 6:  Composing and Writing 
The writing process; revision vs. editing; punctuation, spelling, and commonly confused words; writing a paragraph.

Recommended Background
The course is designed for three main types of learners: 
College bound, underprepared students: The course provides fundamental writing skills for college bound students prior to taking placement tests, saving them from spending unwarranted time and money for remediation should they be placed below transfer level. Successful completion of the course may be accepted as an alternative to a placement test.
Current high school and college students in regular classes: This course provides writing support for any student taking any course at any time.
Global Community Members: This course will assist people in improving their writing skills to satisfy their need to be productive citizens in a global society.
Course Format
The format of the course includes video teaching sessions by your professors with real students, writing exercises and activities, discussions with faculty and tutor team members, and opportunities for meet up sessions using Blackboard Collaborate and/or Google Hangouts. 

The course is six weeks long and will require about 4-5 hours per week of your time to complete.   The first weeks may require less of your time to complete than weeks 4 - 6.  Please plan accordingly.
FAQ
Q: Are there any prerequisites? 
A: No. All you need is a willingness to grow as a writer and communicator.

Q: I really want to learn to correct the mistakes I always make when writing. Is this the class for me? 
A: Yes, but we would like for you to focus on becoming a stronger writer first. If you know how to craft a sentence correctly, you will not be making the same errors AND can free yourself to become a strong communicator.

Q: Who is the intended audience for this course? 
A: We designed this course for the basic writer, a person who would place a couple of classes below English composition at an American college or university. 

Q: I've written a novel, have my B.A. in English, and I write a regular blog. Is this class for me? 
A: Maybe. If you want to hone your grammar skills and improve as a writer in the context of a class that includes many different levels of writers, and you are willing to help others improve their writing, then yes. If you are looking for an advanced grammar class, then we encourage you create a discussion group within the course where you may work with individuals of the same interest and skill level. 

Q: Will I learn everything there is to know about grammar in this course?

A: No, but you will learn as much as you need to write a well-written, focused, and detailed paragraph. Throughout the course, we have suggested websites and texts that you can reference for further information about any of the topics covered. Also, you may connect with others who have a similar interest in grammar

Q: Do I need to purchase any textbooks for the course? 
A: No. However, if you would like a book, find a writing reference book that you like to keep on your desk as you continue to develop as a writer! Professor Larry Barkley, one of the faculty for this course, has written a grammar text entitled Grammar and Usage, Naturally available from Cengage. 

Q: Can I get college credit for this course? 
A: No. This course was designed to prepare students for basic writing courses and/or to refresh their skills. However, the completion  of this course may give you credit towards advancing to higher level English courses.  Check with the school you are planning to attend for its recommendation about this issue.

Q: Will I get a statement of accomplishment after taking this course? 
A: Yes. Students who successfully complete this course will receive a statement of accomplishment including a final writing sample from the course.

Q: Can online students ask questions and/or contact the professor?
A: Yes, but not directly. There are discussion forums within the course where students rank questions and answers. The most important questions and the best answers "bubble" to the top. Teaching staff will monitor these forums, so that important questions not answered by other students can be addressed by faculty. 






Sessions

Join for Free
Course at a Glance
5 weeks
4-5 hours of work / week
English
English, Chinese subtitles
Instructors

Lawrence (Larry) Barkley
Mt. San Jacinto College
Ted Blake
Mt. San Jacinto College
Lorrie Ross
Mt. San Jacinto College
Categories
Humanities 
Share

Related Courses

Applying to U.S. Universities

Reason and Persuasion: Thinking Through Three Dialogues By Plato

Creative Problem Solving
Browse more courses
Coursera empowers people to improve their lives, the lives of their families, and the communities they live in with education.

© 2014 Coursera Inc. All rights reserved.

COMPANY

About
People
Leadership
Careers
FRIENDS

Partners
Community
Programs
CONNECT

Google+
Twitter
Facebook
Blog
Store
MORE

Terms
Privacy
Help
Press
Contact
