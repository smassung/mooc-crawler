|Global Partners
CoursesSpecializationsNewInstitutionsAbout ▾ |Sign InSign Up

Guinea Pigs, Heroes & Desperate Patients: The History & Ethics of Human Research
Learn about the ethical issues that arise when conducting human subjects research, as well as the history that grounds policies and debates in this area of biomedicine.


About the Course
The objective of this course is to increase participants’ awareness and understanding of ethical issues that arise in the conduct of research on human subjects. The course will explore the history and future of ethical and policy challenges using a multi-disciplinary approach. Leading scholars in the ethics of human subject research will serve as course faculty.  This course will introduce participants to the history that grounds policies and debates regarding the use of humans in research. It will continue with discussion of how current international and US policies governing research are applied, how ethical norms and concepts have evolved, and how, in some cases, changes in research have outpaced changes in policy, leading to an often complicated mismatch between the two. 

The lead instructor is Jeffrey Kahn, who is the Robert Henry Levi and Ryda Hecht Levi Professor of Bioethics and Public Policy at the Johns Hopkins University Berman Institute of Bioethics.  He will be joined by colleagues who are all among the leading scholars in aspects of the ethical and policy issues related to research on human subjects, including Profs. Ruth Faden (history and theory of informed consent), Nancy Kass (public health research, research in international settings), Jeremy Sugarman (empirical approaches to research ethics, US research policy), Debra Mathews (US and international science and research policy), Gail Geller (participants' perspectives on research participation), Joseph Ali (law and research oversight in US and international settings), and Dan O'Connor (medical history, ethics and social media-based research). 
Recommended Background
No background required--all materials are in English, so English reading and speaking proficiency is necessary.

Suggested Readings
Only public domain or open access readings will be required.  A non-required background reading list will be provided for those who would like additional materials or greater depth in particular topic areas.

Course Format
The class will consist of short (8-12 minute) topical modules that will be a combination of lecture, video content, and interviews.  Each module includes 1-3 integrated quiz and discussion questions.

FAQ
·  Will I get a Statement of Accomplishment after completing this class?

Yes. Students who successfully complete the class will receive a Statement of Accomplishment. 

·  What resources will I need for this class?

For this course, all you need is an Internet connection, copies of the suggested readings (all of which can be obtained online for free), and the time to read, write, discuss, and enjoy thinking about the interesting issues raised by the lectures and materials.  

·  What is the most important thing I'll learn if I take this class?

Working with world class scholars, students will gain a greater appreciation of the ethics of research on humans, and the unique circumstances and personalities that shaped our present combination of ethical principles, oversight policies, and practices.

No sessions available
Add to Watchlist
Course at a Glance
3-4 hours of work / week
English
English subtitles
Instructors

Jeffrey Kahn
Johns Hopkins University
Alan C Regenberg
Johns Hopkins University
Debra JH Mathews
Johns Hopkins University
Joseph Ali
Johns Hopkins University
Categories
Medicine
Humanities 
Health & Society
Biology & Life Sciences
Social Sciences
Share

Related Courses

Practical Ethics

Epidemiology: The Basic Science of Public Health

University Teaching 101
Browse more courses
Coursera empowers people to improve their lives, the lives of their families, and the communities they live in with education.

© 2014 Coursera Inc. All rights reserved.

COMPANY

About
People
Leadership
Careers
FRIENDS

Partners
Community
Programs
CONNECT

Google+
Twitter
Facebook
Blog
Store
MORE

Terms
Privacy
Help
Press
Contact
