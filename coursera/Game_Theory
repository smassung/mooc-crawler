|Global Partners
CoursesSpecializationsNewInstitutionsAbout ▾ |Sign InSign Up

Game Theory
The course covers the basics: representing games and strategies, the extensive form (which computer scientists call game trees), repeated and stochastic games, coalitional games, and Bayesian games (modeling things like auctions).

Preview Lectures

Watch intro video
About the Course
Popularized by movies such as "A Beautiful Mind", game theory is the mathematical modeling of strategic interaction among rational (and irrational) agents. Beyond what we call 'games' in common language, such as chess, poker, soccer, etc., it includes the modeling of conflict among nations, political campaigns, competition among firms, and trading behavior in markets such as the NYSE. How could you begin to model eBay, Google keyword auctions, and peer to peer file-sharing networks, without accounting for the incentives of the people using them? The course will provide the basics: representing games and strategies, the extensive form (which computer scientists call game trees), Bayesian games (modeling things like auctions), repeated and stochastic games, and more. We'll include a variety of examples including classic games and real-world applications.
Course Syllabus
Week 1. Introduction:  Introduction, overview, uses of game theory, some applications and examples, and formal definitions of: the normal form, payoffs, strategies, pure strategy Nash equilibrium, dominated strategies.

Week 2. Mixed-strategy Nash equilibria: Definitions, examples, real-world evidence.

Week 3. Alternate solution concepts: iterative removal of strictly dominated strategies, minimax strategies and the minimax theorem for zero-sum game, correlated equilibria.

Week 4. Extensive-form games: Perfect information games: trees, players assigned to nodes, payoffs, backward Induction, subgame perfect equilibrium, introduction to imperfect-information games, mixed versus behavioral strategies.

Week 5. Repeated games: Repeated prisoners dilemma, finite and infinite repeated games, limited-average versus future-discounted reward, folk theorems, stochastic games and learning.

Week 6. Coalitional games: Transferable utility cooperative games, Shapley value, Core, applications.

Week 7. Bayesian games: General definitions, ex ante/interim Bayesian Nash equilibrium.

Recommended Background
You must be comfortable with mathematical thinking and rigorous arguments. Relatively little specific math is required; the course involves lightweight probability theory (for example, you should know what a conditional probability is) and very lightweight calculus (for instance, taking a derivative).
Suggested Readings
The following background readings provide more detailed coverage of the course material:
Essentials of Game Theory, by Kevin Leyton-Brown and Yoav Shoham; Morgan and Claypool Publishers, 2008. This book has the same structure as the course, and covers most of the same material. It is free if you access the link from a school that subscribes to the Morgan & Claypool Synthesis Lectures, and otherwise costs $5 to download. You can also get it as a printed book from (e.g.) amazon.com, or as an ebook for Kindle or Google devices.
A Brief Introduction to the Basics of Game Theory, by Matthew O. Jackson. These notes offer a quick introduction to the basics of game theory; they are available as a free PDF download.
Course Format
The course consists of the following materials:

Videos.  The lectures are delivered via videos, which are broken into small chunks, usually between five and fifteen minutes each. There will be approximately one and a half hours of video content per week. You may watch the lecture videos at your convenience. Lower-resolution videos are also available for those with slow internet connections.
Slides.  We have made available pdf files of all the lecture slides.
Quizzes.  There will be non-graded short "quiz" questions that will follow some of the videos to help you gauge your understanding.
Online Lab Exercises  After some of the videos, we will ask you to go online to play some games. These are entirely optional, and are designed to illustrate some of the concepts from the course.
Problem Sets.  There will also be graded weekly problem sets that you will also answer online, but may work through offline; those must be completed within two weeks of the time that they are posted in order to be graded for full credit. If you miss a problem set deadline, you may complete it before the end of the course for half credit. You may discuss problems from the problem sets with other students in an online forum, without providing explicit answers.
Final Exam.  There will be an online final exam that you will have to complete within two weeks of its posting. Once you begin the exam, you will have four hours to complete it.
Screen-side Chats.  A couple of times during the course, we will hold a brief online chat where we answer  questions and discuss topics relevant to the course.
FAQ
Will I get a statement of accomplishment after completing this class?
Yes. Students who successfully complete the class will receive a statement of accomplishment signed by the instructors.

Sessions

Join for Free
Course at a Glance
9 weeks
5-7 hours of work / week
English
English subtitles
Instructors

Matthew O. Jackson
Stanford University
Kevin Leyton-Brown
The University of British Columbia
Yoav Shoham
Stanford University
Categories
Computer Science: Theory
Economics & Finance
Computer Science: Artificial Intelligence
Share

Related Courses

Game Theory II: Advanced Applications

Model Thinking

General Game Playing
Browse more courses
Coursera empowers people to improve their lives, the lives of their families, and the communities they live in with education.

© 2014 Coursera Inc. All rights reserved.

COMPANY

About
People
Leadership
Careers
FRIENDS

Partners
Community
Programs
CONNECT

Google+
Twitter
Facebook
Blog
Store
MORE

Terms
Privacy
Help
Press
Contact
