|Global Partners
CoursesSpecializationsNewInstitutionsAbout ▾ |Sign InSign Up

Grow to Greatness: Smart Growth for Private Businesses, Part II
This course focuses on the common human resource ("people") challenges faced by existing private businesses when they attempt to grow substantially. PART 1 OF THE GROW TO GREATNESS COURSE IS NOT A PREREQUISITE FOR TAKING THIS COURSE.


Watch intro video
About the Course
Most entrepreneurship courses focus on how to start a business. Few focus on the next big entrepreneurial inflection point: how do you successfully grow an existing private business? This is the focus of this Course. It is based on the instructor's research and thirty years of real-world experience advising private growth companies.
 
This Course will focus on the common “people” challenges private growth companies face as they grow. You will study stories of how six different private businesses faced their growth challenges.
 
While strategic focus and operational excellence are necessary to build a great growth company, they are not sufficient. Growth requires the right kind of leadership, culture, and people. My research clearly showed that many entrepreneurs struggle with personal challenges presented to them by growth, as well as the challenge of hiring the right people and building the right management team that can play well together. The research shows that every growth business faces common challenges. You can learn from others' experience—you do not have to "reinvent the wheel".
 
The Course format is story based. Each case tells a compelling story. You will learn from Barbara Lynch, Ryan Dienst, Steve Ritter, Randy Bufford, John Gabbert, and Mike Cote. In addition, each week, we will discuss a different content theme. In Week 3, you will engage in a Workshop where you will be asked to apply the Growth System Assessment Tool. You will have the opportunity to create a Course Community of fellow students to learn from each other as the Course progresses.
 
You will learn how entrepreneurs must grow, too; the “secret” of high performance; people-centric leadership; how to create high employee engagement; how to create an internal Growth System; and how to build a senior management team.
Course Syllabus
Please see Syllabus for further detail on weekly reading and assignments.
 
Week 1: The Entrepreneur Must Grow, Too! In order for a business to grow, the entrepreneur must grow. First, the entrepreneur’s role will evolve from primarily being a “doer” to a manager then to a leader and ultimately to being a coach/mentor. Additionally the entrepreneur in most cases moves from being a specialist to a generalist to ultimately being a “conductor”. We will learn from Barbara Lynch, a much-heralded Chef who, with little formal education or training, built a multi-concept restaurant “empire” in Boston, and from Ryan Dienst, one of the founders of Global Medical Imaging.
 
Week 2: The “Secret” of High Performance is High Employee Engagement. Using the Leaders Bank and Trilogy Health Services stories we will look at how two businesses put in place people policies, practices, and a leadership model that led to high employee engagement, loyalty, and productivity that drove high customer satisfaction. We will also focus on the principles of people-centric leadership. During this Week, you will engage with Community members in Workshop learning.
 
Week 3: Growth Is Much More Than a Strategy—It Requires a SYSTEM. Growth is behavioral. To enable and promote the desired behaviors, entrepreneurs have to create an internal aligned system that links, in a consistent self-reinforcing manner, strategy/business model, culture, structure, leadership behaviors, measurements, and rewards. This week we will learn from John Gabbert, who built highly successful retail home furnishings chain Room & Board his way, rejecting the common business practices of his industry. My MBA students find this story mindboggling. If you have work experience, you will also use the Growth System Assessment Tool.
 
Week 4: The Surprising Difficulties in Building a Senior Management Team.  I was surprised in my research at how difficult it was for entrepreneur’s to build a senior management team. In many cases it took multiple hirings to get the right fit. And even then, getting the senior management team to “play well together” was challenging. Lastly, most entrepreneurs were not prepared for the need to “upgrade” their management team as the business grew. This week we will study how Mike Cote took over as CEO of SecureWorks, a floundering Internet security company, and grew it into an industry leader ultimately selling the company last year to Dell for hundreds of millions of dollars.
Recommended Background
You may attend this Course even if you did not take Grow to Greatness, Part 1. The only prerequisite for this Course is an interest in learning about how privately owned entrepreneurial businesses grow. The following people will find this course helpful: students at all levels, private business owners, managers of private businesses, employees of private businesses, and people interested in growth, economic development, and job creation.
Suggested Readings
All of the required Course readings are listed in the Syllabus and will be provided for free by the University of Virginia Darden School Foundation.
 
Additional reading that you might find helpful for the Course includes:
 
        1. Edward D. Hess, Grow to Greatness: Smart Growth for Entrepreneurial Businesses, Stanford, CA: Stanford Business Books, 2012, which is the book that this Course is based on. 
 
        2. Edward D. Hess and Charles F. Goetz, So! You Want to Start a Business?, Upper Saddle River, NJ: FT Press, 2008. This book was written for people who want to start a business. It focuses on the eight common reasons why start-ups fail and how to increase your probability of avoiding those mistakes.
 
        3. Edward D. Hess, The Road to Organic Growth: How Great Companies Consistently Grow Marketshare From Within, New York: McGraw Hill, 2007. This book is based on a study of consistent high organic growth U.S. public companies and their defining characteristics—the six keys to their success. Company stories include Sysco, Stryker Corporation, Outback Steakhouse, Best Buy Co., Inc., TSYS, Tiffany & Co., and American Eagle Outfitters, Inc..
 
My additional list of good reads (not required):
 
        1. Marcus Buckingham and Curt Coffman, First, Break All the Rules: What the World’s Greatest Managers Do Differently, New York: Simon & Schuster, 1999.
       
        2. Kim S. Cameron, Positive Leadership: Strategies for Extraordinary Performance, San Francisco, CA: Berrett-Koehler, 2008.
 
        3.  James C. Collins and Jerry I. Porras, Built to Last: Successful Habits of Visionary Companies, New York: HaperCollins, 1994.
 
        4. Peter F. Drucker, Innovation and Entrepreneurship, New York: HarperBusiness, 1993.
 
        5. Michael E. Gerber, The E-Myth Revisited: Why Most Small Businesses Don’t Work and What to Do About It, New York: HarperCollins, 2001.
 
        6. Charles A. O’Reilly, III and Jeffrey Pfeffer, Hidden Value: How Great Companies Achieve Extraordinary Results with Ordinary People, Boston: Harvard Business School Press, 2000.
 
        7. Robert I. Sutton, Good Boss, Bad Boss: How to Be the Best—and Learn from the Worst, New York: Business Plus, 2010. 
Course Format
Videos and Workshops:
Each week consists of lecture videos made for the Course in short segments of approximately 10 to 20 minutes each. The videos discuss the articles and case studies to be read prior to the class. There are quizzes embedded within the lecture videos to evaluate your understanding of the concepts. You may proceed at your own pace.
 
There is a (non-optional) final exam of multiple choice questions.
 
Course Communities
You may choose to create a Course Community, which will allow you to learn from your classmates. Communities can be formed based on your country of residence or based on the type of business you own or want to learn about. The purpose of the community is to provide a forum for you to learn from each other. It’s an opportunity for you create and grow together.
 
Instructor Engagement
I will hold "office hours" every Wednesday morning during the Course from 10:00am to 11:30am EST where I will be on the Course Discussion Forum to answer questions, chat and respond to other conversations. 
FAQ
How will I learn in this Course?
This Course is based on the principle that one learns by doing. There are required and optional readings. You will be asked to apply concepts discussed in the videos and readings in Workshop exercises, and if you choose to join a Course Community, you will learn from each other.

What resources will I need for this Course?
None. The Darden Graduate School of Business at the University of Virginia is providing this Course for free because of its mission to positively impact society. Darden Business Publishing will provide all required Course reading for free.

Why is this Course important?
Growing private businesses is in many parts of the world a major job creator and a key route to building a better life for oneself and one's family. In effect, this Course is about the pursuit of hopes and dreams. I think doing business is much more than just about making money. Doing business is the primary way many people achieve their hopes and dreams to provide a better life for themselves and their families. And in doing so, building a business can also create value for employees, customers, and society. Building a business requires the right strategic focus, a compelling constantly improved customer value proposition, and the right leadership that can create a high engagement high performance environment with the right team.

What is the coolest thing I'll learn if I take this Course?
This course is really a practical course on leadership: learning how to engage others in pursuits they find meaningful in such a way that consistent high performance and excellence becomes the norm. No matter what you do in life, you will be dependent on other people, and this Course should be useful in both your personal and work life.

Will I receive a Statement of Accomplishment after completing this Course?
Yes. Students who successfully complete the Course will receive a Statement of Accomplishment signed by the instructor.

Sessions

Join for Free
Course at a Glance
4 weeks
4-6 hours of work / week
English
English subtitles
Instructors

Edward D. Hess
University of Virginia
Categories
Business & Management
Share

Related Courses

Éléments de Géomatique

Grow to Greatness: Smart Growth for Private Businesses, Part I

Developing Innovative Ideas for New Companies: The First Step in Entrepreneurship
Browse more courses
Coursera empowers people to improve their lives, the lives of their families, and the communities they live in with education.

© 2014 Coursera Inc. All rights reserved.

COMPANY

About
People
Leadership
Careers
FRIENDS

Partners
Community
Programs
CONNECT

Google+
Twitter
Facebook
Blog
Store
MORE

Terms
Privacy
Help
Press
Contact
