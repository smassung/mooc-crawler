|Global Partners
CoursesSpecializationsNewInstitutionsAbout ▾ |Sign InSign Up

The Hardware/Software Interface
Examines key computational abstraction levels below modern high-level languages. From Java/C to assembly programming, to basic processor and system organization.

Preview Lectures

About the Course
This course examines key computational abstraction levels below modern high-level languages; number representation, assembly language, introduction to C, memory management, the operating-system process model, high-level machine architecture including the memory hierarchy, and how high-level languages are implemented. We will develop students’ sense of “what really happens” when software runs — and that this question can be answered at several levels of abstraction, including the hardware architecture level, the assembly level, the C programming level and the Java programming level. The core around which the course is built is C, assembly, and low-level data representation, but this is connected to higher levels (roughly how basic Java could be implemented), lower levels (the general structure of a processor and the memory hierarchy), and the role of the operating system (but not how the operating system is implemented).
Course Syllabus
This course should develop students’ sense of “what really happens” when software runs — and convey that this question can be answered at several levels of abstraction, including the hardware architecture level, the assembly level, the C programming level and the Java programming level. The core around which the course is built is C, assembly, and low-level data representation, but this is connected to higher levels (roughly how basic Java could be implemented), lower levels (the general structure of a processor), and the role of the operating system (but not how the operating system is implemented). For (computer science) students wanting to specialize at higher levels of abstraction, this could in the extreme be the only course they take that considers the “C level” and below. However, most will take a subset of Systems Programming, Hardware Design and Implementation, Operating Systems, Compilers, etc. For students interested in hardware, embedded systems, computer engineering, computer architecture, etc., this course is the introductory course after which other courses will delve both deeper (into specific topics) and lower (into hardware implementation, circuit design, etc.). The course has three principal themes:
Representation: how different data types (from simple integers to arrays of data structures) are represented in memory, how instructions are encoded, and how memory addresses (pointers) are generated and used to create complex structures.
Translation: how high-level languages are translated into the basic instructions embodied in process hardware with a particular focus on C and Java.
Control flow: how computers organize the order of their computations, keep track of where they are in large programs, and provide the illusion of multiple processes executing in parallel.
At the end of this course, students should:
understand the multi-step process by which a high-level program becomes a stream of instructions executed by a processor;
know what a pointer is and how to use it in manipulating complex data structures;
be facile enough with assembly programming (X86) to write simple pieces of code and understand how it maps to high-level languages (and vice-versa);
understand the basic organization and parameters of memory hierarchy and its importance for system performance;
be able to explain the role of an operating system;
know how Java fundamentally differs from C;
grasp what parallelism is and why it is important at the system level; and
be more effective programmers (more efficient at finding bugs, improved intuition about system performance).
Topics: 
Number representation
Assembly language
Basics of C
Memory management
Operating-system process model
High-level machine architecture
Memory hierarchy
Implementation of high-level languages
Recommended Background
Introductory programming in C or Java.
Suggested Readings
Text are recommended but not required:
Computer Systems: A Programmer’s Perspective, 2nd Edition (CS:APP2e) 
Randal E. Bryant and David R. O’Hallaron 
Prentice-Hall, 2010

Purchase direct from Pearson 
Purchase eBook from CourseSmart 
Purchase print or Kindle edition from Amazon.com

Students are also encouraged to have access to a good C reference – any will do – there are many available on the web as well:

The C Programming Language (Kernighan and Ritchie) 
C: A Reference Manual (Harbison and Steele)

Course Format
Video lecture topics. Written homework and programming assignments. Only the programming assignments will be graded. The written assignments provide students with a sense for the kinds of topics and analysis are considered most important.
No sessions available
Add to Watchlist
Course at a Glance
10-15 hours of work / week
English
English subtitles
Instructors

Gaetano Borriello
University of Washington
Luis Ceze
University of Washington
Categories
Computer Science: Systems & Security
Share

Related Courses

Bioinformatic Methods II

VLSI CAD: Logic to Layout

Computer Architecture
Browse more courses
Coursera empowers people to improve their lives, the lives of their families, and the communities they live in with education.

© 2014 Coursera Inc. All rights reserved.

COMPANY

About
People
Leadership
Careers
FRIENDS

Partners
Community
Programs
CONNECT

Google+
Twitter
Facebook
Blog
Store
MORE

Terms
Privacy
Help
Press
Contact
