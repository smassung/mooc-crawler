|Global Partners
CoursesSpecializationsNewInstitutionsAbout ▾ |Sign InSign Up

Understanding and Improving the US Healthcare System
In this course, you will learn about the structure, accomplishments, and shortcomings of the US healthcare system, and how those have prompted attempts at reform. You will also take part in a unique, national group exercise, designed to help you understand how you can improve the US healthcare system.


Watch intro video
About the Course
Health is important to everyone.  For most people, that means that understanding the healthcare system is important.  As a patient or a caregiver for a loved one, your understanding of the system can mean everything from getting the right help, to economic stress in paying medical bills, to medical misadventures and even death.  And if you’re a healthcare professional (or are in training or want to be a healthcare professional someday), understanding the system is a critical – but often under-developed – part of optimizing the help you can provide your community.

 

Despite this importance, an understanding of the US healthcare system is alarmingly rare.  This course is designed to change that – by helping students understand the core structure and accomplishments of the system, the recurring shortcomings, and attempted remedies through policy reform.  Rather than emphasizing complexity (a focus of many other courses about healthcare policy), the lessons in this course will highlight central themes that learners can take away from the course and apply to their own experiences – and other coursework – with confidence.  Moreover, students who seek more advanced content will have ample opportunity to tailor their learning accordingly.

 

In addition, through a unique, national group exercise that will serve as a keystone of this course, all learners will have the opportunity to learn how they can improve the US healthcare system.  Whether as individuals, in groups, or as future architects of system reform, students in this course will enjoy a premier opportunity to understand the US healthcare system – and their options for addressing problems in the system – as they never have before.  This course is produced by Michael Rubyan, MPH, one of Dr. Davis's former students at the University of Michigan.

Recommended Background
No prerequisites required; all are welcome as this course will bring principles and challenges of the US healthcare system to learners in an accessible way, and will encourage them to examine their own assumptions and experiences as they learn-through-innovation.  The materials are designed to meet the needs and expectation of learners with a broad array of prior experiences in, and knowledge about, the healthcare system.  

Suggested Readings
A small number of primary source readings from the health policy literature will be posted online for students to read and prepare in advance of weekly didactic teaching sessions and learn-through-innovation.
Course Format
In general, each week will consist of approximately 60 minutes of didactic material, delivered in 8- to 15-minute video segments comprised of lecture, thought exercises, and rare archival footage.  Students will be expected to view the video materials, which will include segments that can be tailored to students’ particular interests (including advanced learners).  Each week will also include some work outside of video viewing – to include homework assignments focused on reflecting about and sharing thoughts on the US healthcare system, readings assigned by the instructor, and self-assessments.

 

The exception to this time allotment will occur in Weeks 4 and 5, when students will be randomly assigned to groups for purposes of the group exercise.  The exercise will include expectation of communication with other members of each student’s group, in order to plan benefits in a health plan; time spent on communication will take the place of homework during those weeks
FAQ
Q:  Will a certificate be awarded for this course?

A:  YES – Upon timely completion of two self-evaluations (pre-course; mid-course) and the course evaluation at course end, as well as submission of the plan for health benefits design for each student’s stakeholder group, a student will be eligible to receive a signed Statement of Accomplishment.

Q:  If I have already taken, or plan to take, coursework in health policy elsewhere, is this course still for me?

A:  YES – The goal of this course is to bring learners together from a variety of disciplines, which would be unusual for coursework in just about any other setting.  Moreover, the emphasis in this course is on central themes, rather than complexity that is the hallmark of many other courses in health policy.  Furthermore, a major goal of this course is to help you understand how you can improve the system – in other words, to help each student have a personal, practical, take-away lesson, to complement their improved understanding of the system.

Sessions

Join for Free
Course at a Glance
6 weeks
3-4 hours of work / week
English
English subtitles
Instructors

Matthew Davis
University of Michigan
Categories
Medicine
Health & Society
Share

Related Courses

Age of Jefferson

Statistical Analysis of fMRI Data

Health Informatics in the Cloud
Browse more courses
Coursera empowers people to improve their lives, the lives of their families, and the communities they live in with education.

© 2014 Coursera Inc. All rights reserved.

COMPANY

About
People
Leadership
Careers
FRIENDS

Partners
Community
Programs
CONNECT

Google+
Twitter
Facebook
Blog
Store
MORE

Terms
Privacy
Help
Press
Contact
