var page = require('webpage').create();
var system = require('system');
var fs = require('fs');

if(system.args.length !== 3) {
  console.log('Usage: phantomjs text-scraper.js <url> <output>');
  phantom.exit();
}

var url = system.args[1];
var outfile = system.args[2];

page.onConsoleMessage = function(msg) {
  console.log(msg);
};

fs.write(outfile, url + '\n');
page.open(url, function(status) {
  if(status === 'success') {
    setTimeout(function() {
      var text = page.evaluate(function() {
        var text = document.title + '\n';
        text += document.body.innerText + '\n';
        console.log("done?");
        return text;
      });
      fs.write(outfile, text);
    }, 1000);
    setTimeout(function() {
      phantom.exit()
    });
  } else {
    console.log("Error!")
    phantom.exit()
  }
});
