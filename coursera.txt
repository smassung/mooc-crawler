Fundamentals of Audio and Music Engineering: Part 1 Musical Sound & Electronics
Introductory Physics I with Laboratory
Model Thinking
Introduction to Mathematical Thinking
Instructional Methods in Health Professions Education
Marine Megafauna: An Introduction to Marine Science and Conservation
Imagining Other Earths
Foundations of Teaching for Learning 4: Curriculum
Developing Innovative Ideas for New Companies: The First Step in Entrepreneurship
Introduction to Thermodynamics: Transferring Energy from Here to There
Globalization of Business Enterprise
Scandinavian Film and Television
Internet History, Technology, and Security
Property and Liability: An Introduction to Law and Economics
Fantasy and Science Fiction: The Human Mind, Our Modern World
Introduction to Finance
Analysis of Algorithms
Crafting an Effective Writer: Tools of the Trade
The Music of the Beatles
Teaching Character and Creating Positive Classrooms
Latin American Migration
Student Thinking at the Core
Introduction to Clinical Neurology
Interprofessional Healthcare Informatics
Reason and Persuasion: Thinking Through Three Dialogues By Plato
Citizenship and U.S. Immigration
Financial Markets
Data Analysis and Statistical Inference
Digital Systems - Sistemas Digitales: De las puertas l  gicas al procesador
Design and Interpretation of Clinical Trials
Beauty, Form & Function: An Exploration of Symmetry
Age of Jefferson
Epidemiology: The Basic Science of Public Health
Understanding Europe: Why It Matters and What It Can Offer You
Building an Information Risk Management Toolkit
Introduction to Computational Finance and Financial Econometrics
Information Security and Risk Management in Context
Designing and Executing Information Security Strategies
Confronting The Big Questions: Highlights of Modern Astronomy
Practicing Tolerance in a Religious Society: The Church and the Jews in Italy
Live!: A History of Art for Artists, Animators and Gamers
Genetics and Society: A Course for Educators
Archaeology's Dirty Little Secrets
Statistical Analysis of fMRI Data
Nutrition for Health Promotion and Disease Prevention
Creative Problem Solving
TechniCity
Introduction to International Criminal Law
Practical Ethics
Networks: Friends, Money, and Bytes
Calculus Two: Sequences and Series
VLSI CAD: Logic to Layout
Machine Learning
Mathematical Biostatistics Boot Camp 1
Nanotechnology and Nanosensors
A Look at Nuclear Science and Technology
Bioinformatic Methods II
Art and Inquiry: Museum Teaching Strategies For Your Classroom
An Introduction to Operations Management
Discrete Optimization
Logic: Language and Information 1
Engineering Systems in Motion: Dynamics of Particles and Bodies in 2D Motion
A Beginner's Guide to Irrational Behavior
Exploring Beethoven   s Piano Sonatas
Music's Big Bang: The Genesis of Rock 'n' Roll
The Science of Gastronomy
Massively Multivariable Open Online Calculus Course
Applying to U.S. Universities
Grow to Greatness: Smart Growth for Private Businesses, Part II
Foundations of Teaching for Learning 2: Being a Teacher
Common Core in Action: Math Formative Assessment
University Teaching 101
Compilers
Buddhist Meditation and the Modern World
The Emergence of the Modern Middle East
Buddhism and Modern Psychology
Algorithms, Part II
Globalizing Higher Education and Research for the    Knowledge Economy
Foundations of Teaching for Learning 5: Planning for Teaching and Learning
Understanding economic policymaking
An Introduction to Interactive Programming in Python
Diabetes - a Global Challenge
Analytic Combinatorics
Global Warming: The Science of Climate Change
High Performance Scientific Computing
Fundamentals of Digital Image and Video Processing
Caries Management by Risk Assessment (CAMBRA)
Poisonings in the Home and Community: Assessment and Emergency Response
Common Core in Action: Literacy Across Content Areas
International Human Rights Law: Prospects and Challenges
General Game Playing
Dynamical Modeling Methods for Systems Biology
Introduction to Public Speaking
Globalization and You
Introduction to Systems Biology
Understanding the Brain: The Neurobiology of Everyday Life
Understanding Russians: Contexts of Intercultural Communication
Malicious Software and its Underground Economy: Two Sides to Every Story
Introduction to Systems Engineering
Introduction to Mathematical Philosophy
New Models of Business in Society
Coaching Teachers: Promoting Changes that Stick
Core Concepts in Data Analysis
AstroTech: The Science and Technology behind Astronomical Discovery
Developing Your Musicianship
Public Economics
Pattern-Oriented Software Architectures: Programming Mobile Services for Android Handheld Systems
Everything is the Same: Modeling Engineered Systems
Inquiry Science Learning: Perspectives & Practices 4 - Student-Centered Inquiry
Getting and Cleaning Data
The New Nordic Diet - from Gastronomy to Health
R Programming
The Data Scientist   s Toolbox
Climate Change in Four Dimensions
An Introduction to Population Health
Genomic and Precision Medicine
The Dynamic Earth: A Course for Educators
Performance Assessment in the Virtual Classroom
English Composition I: Achieving Expertise
Warhol
Community Change in Public Health
Design: Creation of Artifacts in Society
Technology and Ethics
Logic: Language and Information 2
Epigenetic Control of Gene Expression
Beyond Silicon Valley: Growing Entrepreneurship in Transitioning Economies
Sports and Building Aerodynamics
Maps and the Geospatial Revolution
Introduction to Forensic Science
The Diversity of Exoplanets
Case-Based Introduction to Biostatistics
The Global Student's Introduction to U.S. Law
Introduction to Human Behavioral Genetics
EDIVET: Do you have what it takes to be a veterinarian?
Reproducible Research
Analyzing Global Trends for Business and Society
Exploratory Data Analysis
Statistical Inference
Using The Next Generation Science Standards for Students    Deeper Understanding
Foundations of Teaching for Learning 3: Learners and Learning
The Changing Global Order
Foundations of Teaching for Learning 6: Introduction to Student Assessment
Mathematical Methods for Quantitative Finance
Software Defined Networking
Water Supply and Sanitation Policy in Developing Countries
Enhance Your Career and Employability Skills
ICT in Primary Education
The Horse Course: Introduction to Basic Care and Management
Introduction to Neuroeconomics: how the brain makes decisions
Paradoxes of War
Understanding Research Methods
The Olympic Games and the Media
Evolution: A Course for Educators
Developing Data Products
Practical Machine Learning
Sustainability in Practice
An Introduction to Global Health
Animal Behaviour
Regression Models
Principles of Computing
Programming Cloud Services for Android Handheld Systems
Nutrition and Physical Activity for Health
The Meat We Eat
Assessment and Teaching of 21st Century Skills
Foundations of Virtual Instruction
Introduction to the Clinical Psychology of Children and Young People
Learning to Teach Online
Fundamentals of Music Theory
Animal Behaviour and Welfare
The French Revolution
Art & Activity: Interactive Strategies for Engaging with Art
Social Psychology
The American Disease: Drugs and Drug Control in the USA
Exercise Physiology: Understanding the Athlete Within
Cryptography II
Latin American Culture
Mechanics: Motion, Forces, Energy and Gravity, from Particles to Planets
Human Trafficking
Usable Security
Personal & Family Financial Planning
Algorithmic Thinking
Emerging Trends & Technologies in the Virtual K-12 Classroom
Unethical Decision Making in Organizations
Philosophy and the Sciences
Configuring the World: A Critical Political Economy Approach
Modern & Contemporary American Poetry
Cryptography
Foundations of Teaching for Learning 7: Being a Professional
Introduction to Logic
Origins - Formation of the Universe, Solar System, Earth and Life
S  ren Kierkegaard - Subjectivity, Irony and the Crisis of Modernity
Software Security
What a Plant Knows (and other things you didn   t know about plants)
The Fall and Rise of Jerusalem
Experimental Methods in Systems Biology
Foundations of Teaching for Learning 8: Developing Relationships
International Organizations Management
Hardware Security
Network Analysis in Systems Biology
Integrated Analysis in Systems Biology
Writing in the Sciences
Neural Networks for Machine Learning
Bioelectricity: A Quantitative Approach
Natural Language Processing
Aboriginal Worldviews and Education
Chemistry: Concept Development and Application
Intermediate Organic Chemistry - Part 1
MOS Transistors
Learn to Program: Crafting Quality Code
Statistics: Making Sense of Data
Understanding Einstein: The Special Theory of Relativity
Probabilistic Graphical Models
Drug Discovery, Development & Commercialization
Inspiring Leadership through Emotional Intelligence
Introduction to Psychology
Analytical Chemistry / Instrumental Analysis
The Science of Safety in Healthcare
The Law of the European Union: An Introduction
Creative Programming for Digital Media & Mobile Apps
Startup Engineering
The Social Context of Mental Health and Illness
Networks Illustrated: Principles without Calculus
Structure Standing Still: The Statics of Everyday Objects
Volcanic Eruptions: a material science.
Virology I: How Viruses Work
Learn to Program: The Fundamentals
Practical tips to improve Asian American participation in cancer clinical trials
Economics of Money and Banking, Part One
History of Rock, Part One
Introduction to Systematic Program Design - Part 1
Online Games: Literature, New Media, and Narrative
Economic Issues, Food & You
Inquiry Science Learning: Perspectives and Practices 1 - Science Leadership
9/11 and Its Aftermath -- Part I
Organizational Analysis
Statistics One
Computer Architecture
Computing for Data Analysis
Comic Books and Graphic Novels
Climate Literacy: Navigating Climate Change Conversations
Experimental Genome Science
Introduction to Power Electronics
Asset Pricing
Video Games and Learning
Securing Digital Democracy
Sustainable Agricultural Land Management
Social Network Analysis
Understanding and Improving the US Healthcare System
Human-Computer Interaction
Designing Cities
Pre-Calculus
Chemistry: Concept Development and Application Part II
Economics of Money and Banking, Part Two
Game Theory
Care of Elders with Alzheimer's Disease and other Major Neurocognitive Disorders
Blended Learning: Personalizing Education for Students
21st Century American Foreign Policy
Training and Learning Programs for Volunteer Community Health Workers
Going Out on a Limb: The Anatomy of the Upper Limb
Analysis of a Complex Kind
Big Data in Education
An Introduction to Corporate Finance
History of Rock, Part Two
Financial Engineering and Risk Management Part I
Data Analysis
Markets with Frictions
Useful Genetics Part 1
Automata
The Power of Markets
E-learning and Digital Cultures
Bioinformatics Algorithms (Part 1)
Inquiry Science Learning: Perspectives and Practices 2 - Techniques for Success
Nanotechnology: The Basics
Major Depression in the Population: A Public Health Approach
Antimicrobial Stewardship: Optimization of Antibiotic Practices
Creative, Serious and Playful Science of Android Apps
Introduction to Astronomy
Constitutional Struggles in the Muslim World
Useful Genetics Part 2
Drugs and the Brain
Social and Economic Networks: Models and Analysis
Energy, the Environment, and Our Future
Dino 101: Dinosaur Paleobiology
Cryptography I
Computer Networks
The Power of Macroeconomics: Economic Principles in the Real World
Global Sustainable Energy: Past, Present and Future
Linear Circuits
Information Theory
Bioinformatic Methods I
Unpredictable? Randomness, Chance and Free Will
Computational Methods for Data Analysis
Terrorism and Counterterrorism: Comparing Theory and Practice
Heterogeneous Parallel Programming
Mathematical Biostatistics Boot Camp 2
Write Like Mozart: An Introduction to Classical Music Composition
The Power of Microeconomics: Economic Principles in the Real World
Medical Neuroscience
How Viruses Cause Disease
Calculus: Single Variable
Computational Neuroscience
Energy 101
Introduction to Psychology as a Science
Galaxies and Cosmology
Foundations of Business Strategy
Think Again: How to Reason and Argue
Critical Perspectives on Management
Content Strategy for Professionals: Engaging Audiences for Your Organization
How Things Work 1
Child Nutrition and Cooking 2.0
Introduction to Environmental Law and Policy
Computational Molecular Evolution
Modern European Mysticism and Psychological Thought
Subsistence Marketplaces
Artificial Intelligence Planning
Game Theory II: Advanced Applications
The Modern World: Global History since 1760
Introduction to Communication Science
Roman Architecture
Microeconomics Principles
Games without Chance: Combinatorial Game Theory
Critical Thinking in Global Challenges
Fundamentals of Electrical Engineering
Control of Mobile Robots
How to Change the World
Sports and Society
History of the Slave South
Grow to Greatness: Smart Growth for Private Businesses, Part I
Introduction to Sustainability
Image and video processing: From Mars to Hollywood with a stop at the hospital
Moralities of Everyday Life
Nutrition, Health, and Lifestyle: Issues and Insights
Soul Beliefs: Causes and Consequences
Early Renaissance Architecture in Italy: from Alberti to Bramante
What   s Your Big Idea?
Programming Mobile Applications for Android Handheld Systems
Statistical Reasoning for Public Health: Estimation, Inference, & Interpretation
Analyzing the Universe
The Age of Sustainable Development
Statistical Molecular Thermodynamics
Human Evolution: Past and Future
Introduction to Pharmacy
Surviving Your Rookie Year of Teaching: 3 Key Ideas & High Leverage Techniques
Calculus One
How Green Is That Product? An Introduction to Life Cycle Environmental Assessment
Financial Engineering and Risk Management Part II
Gamification
History and Future of (Mostly) Higher Education
Introductory Human Physiology
Inquiry Science Learning: Perspectives and Practices 3 - Science Content Survey
Understanding Research: An Overview for Health Professionals
Constitutional Law
Advanced Instructional Strategies in the Virtual Classroom
Introduction to Guitar
Turn Down the Heat: Why a 4  C Warmer World Must be Avoided
Fundamentals of Electrical Engineering Laboratory
An Introduction to the U.S. Food System: Perspectives from Public Health
Songwriting
Equine Nutrition
Introduction to Philosophy
Making Better Group Decisions: Voting, Judgement Aggregation and Fair Division
Introduction to Music Production
Understanding Terrorism and the Terrorist Threat
The Modern and the Postmodern
Disaster Preparedness
Foundations of Teaching for Learning 1: Introduction
Jazz Improvisation
Health for All Through Primary Health Care
Algorithms, Part I
Metadata: Organizing and Discovering Information
Principles of Economics for Scientists
Recovering the Humankind Past and Saving the Universal Heritage
Introduction to Recommender Systems
An Introduction to Marketing
Surviving Disruptive Technologies
Principles of Microeconomics
Principles of Public Health
Networked Life
Generation Rx: The Science Behind Prescription Drug Abuse
Competitive Strategy
The Ancient Greeks
Introduction to Engineering Mechanics
Astrobiology and the Search for Extraterrestrial Life
Greek and Roman Mythology
A Brief History of Humankind
Synapses, Neurons and Brains
Genes and the Human Condition (From Behavior to Biotechnology)
Saving Lives Millions at a Time: Global Disease Control Policies & Programs
Listening to World Music
Introduction to Genetics and Evolution
Coding the Matrix: Linear Algebra through Computer Science Applications
Introduction to Digital Sound Design
The Hardware/Software Interface
Understanding Media by Understanding Google
AIDS
Clinical Problem Solving
Survey of Music Technology
Linear and Integer Programming
Conditions of War and Peace
From the Big Bang to Dark Energy
"Pay Attention!!" ADHD Through the Lifespan
The Language of Hollywood: Storytelling, Sound, and Color
The Fiction of Relationship
Global Tuberculosis (TB) Clinical Management and Research
Physics 1 for Physical Science Majors
Introduction to Physical Chemistry
Global Health and Humanitarianism
Science from Superheroes to Global Warming
Health Informatics in the Cloud
Accountable Talk  : Conversation that Works
Introduction to Chemistry
Applications in Engineering Mechanics
Introduction to Sociology
Healthcare Innovation and Entrepreneurship
From the Repertoire: Western Music History through Performance
Fundamentals of Rehearsing Music Ensembles
Intermediate Algebra
Computational Photography
Democratic Development
Writing II: Rhetorical Composing
The Global Business of Sports
An Introduction to Financial Accounting
The Kennedy Half Century
Preparation for Introductory Biology: DNA to Organisms
Rationing and Allocating Scarce Medical Resources
Computer Security
Guinea Pigs, Heroes & Desperate Patients: The History & Ethics of Human Research
Science, Technology, and Society in China I: Basic Concepts
First-Year Composition 2.0
FLOW Education: Facilitating Learning through Outdoor Watershed Education
Growing Old Around the Globe
Introduction to Classical Music
Scientific Computing
Diabetes: Diagnosis, Treatment, and Opportunities
Neuroethics
Social Epidemiology
Clinical Terminology for International and U.S. Students
A New History for a New China, 1700-2000: New Data and New Methods, Part 1
A History of the World since 1300
Sustainability of Food Systems: A Global Life Cycle Perspective
Principles of Obesity Economics
International Organizational Behavior and Leadership
Science, Technology, and Society in China II: History of S&T in Chinese Society
Managing Fashion and Luxury Companies
Responding to 9/11
Our Earth: Its Climate, History, and Processes
Science, Technology, and Society in China III: The Present & Policy Implications
Financing and Investing in Infrastructure
Calvin - Histoire et r  ception d'une R  forme
Computational Investing, Part I
Canine Theriogenology for Dog Enthusiasts
Women and the Civil Rights Movement
Tinkering Fundamentals: Integrating Making Activities into Your STEM Classroom
Integrating Engineering Into Your Science Classroom
Effective Classroom Interactions: Supporting Young Children   s Development
Vaccine Trials: Methods and Best Practices
Exploring Quantum Physics
Understanding Violence
First Year Teaching (Elementary Grades) - Success from the Start
Machine Learning
Measuring Causal Effects in the Social Sciences
Planet Earth
An Introduction to Consumer Neuroscience & Neuromarketing
Accounting: The Language of Business
Emergence of Life
Sustainable Food Production Through Livestock Health Management
The Addicted Brain
Pregnancy and Childbirth: A Global Perspective
The Art and Archaeology of Ancient Nubia
The Bible's Prehistory, Purpose, and Political Future
Fundamentals of Online Education: Planning and Application
Leadership in 21st Century Organizations
Basic Behavioral Neurology
The Brain and Space
Social Entrepreneurship
Introductory Organic Chemistry - Part 1
On the Hunt for Feedback: Self-Directed Teacher Improvement
The Brain-Targeted Teaching   Model for 21st Century Schools
The Role of the Renminbi in the International Monetary System
Algorithms: Design and Analysis, Part 1
Principles of Macroeconomics
C++ For C Programmers
Children Acquiring Literacy Naturally
Leading Strategic Innovation in Organizations
Introductory Organic Chemistry - Part 2
Engaging Students through Cooperative Learning
Introduction to Art: Concepts & Techniques
Programming Languages
Natural Language Processing
Creativity, Innovation, and Change
Epidemics - the Dynamics of Infectious Diseases
Intermediate Organic Chemistry - Part 2
The Holocaust
Web Intelligence and Big Data
Cardiac Arrest, Hypothermia, and Resuscitation Science
Fundamentals of Human Nutrition
Fundamentals of Pharmacology
Algorithms: Design and Analysis, Part 2
Vaccines
Contraception: Choices, Culture and Consequences
Introduction to Tissue Engineering
From GPS and Google Maps to Spatial Computing
Global Health: An Interdisciplinary Overview
First Year Teaching (Secondary Grades) - Success from the Start
Data Management for Clinical Research
Climate Change
Programmed cell death
Resilience in Children Exposed to Trauma, Disaster and War: Global Perspectives
The Camera Never Lies
Design Thinking for Business Innovation
Plagues, Witches, and War: The Worlds of Historical Fiction
English Common Law: Structure and Principles
Preventing Chronic Pain: A Human Systems Approach
Fundamentals of Fluid Power
Health Policy and the Affordable Care Act
Generating the Wealth of Nations
Introduction to Data Science
Creating Site-Specific Dance and Performance Works
Introduction to Programming for Musicians and Digital Artists
Passion Driven Statistics
Law and the Entrepreneur
Introduction to Databases
Computer Science 101
Why We Need Psychology
Fundamentals of Global Energy Business
Preparing for the AP* Calculus AB Exam
Preparing for the AP* Statistics Exam
Applying Principles of Behavior in the K-12 Classroom
Beginning Game Programming with C#
K-12 Blended & Online Learning
Advanced Chemistry
Introduction to Computational Arts: Processing
