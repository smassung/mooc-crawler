require 'open-uri'

url_1 = "https://www.udemy.com/courses/"
url_2 = "/?view=list&p="
topics = ["Technology", "Business", "Design", "Arts-and-Photography", "Health-and-Fitness",
  "Lifestyle", "Math-and-Science", "Education", "Languages", "Humanities", "Social-Sciences",
  "Music", "Crafts-and-Hobbies", "Sports", "Games", "Other"]
output = File.open("udemy.txt", 'w')

for topic in topics
  url = "#{url_1}#{topic}#{url_2}"
  max = 30
  max = 121 if topic == "Technology"
  for page in 1..max
    `sleep 1`
    puts "Topic: #{topic}"
    html = nil
    begin
      html = open("#{url}#{page}").read
    rescue
      puts " !! error crawling #{url}#{page}"
      next
    end
    puts " -> crawling page #{page} of #{topic}"
    for m in html.scan(/<a href="https:\/\/www.udemy.com\/([^"]*)"/)
      for link in m
        next if link.include? '&'
        next if link.include? '?'
        next if link.match(/courses/)
        output << "https://www.udemy.com/#{link}" << "\n"
        puts "   -> found #{link}"
      end
    end
  end
end
